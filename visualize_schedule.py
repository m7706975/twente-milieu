import pandas as pd
import xlsxwriter
import numpy as np


def write_schedule(df, gemeente, W, filename):
    # Define excel writer
    writer = pd.ExcelWriter(filename, engine="xlsxwriter")
    df.to_excel(writer, sheet_name="Schedule", index=False)
    worksheet = writer.sheets["Schedule"]
    workbook = writer.book

    # Whole lot of formatting stuff to get the Excel sheet to look nice
    worksheet.set_column('A:A', 12)
    worksheet.set_column('B:B', 20)


    # Formats
    gemeente_format = workbook.add_format({
        'bold': 1,
        'font_color': 'red',
        'border': 2,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#F8CBAD'})

    grijs_format = workbook.add_format({
        'border': 1,
        'fg_color': '#BFBFBF'})

    groen_format = workbook.add_format({
        'border': 1,
        'fg_color': '#92D050'})

    VEC_format = workbook.add_format({
        'border': 1,
        'fg_color': '#FFC000'})

    papier_format = workbook.add_format({
        'border': 1,
        'fg_color': '#00B0F0'})

    empty_format = workbook.add_format({
        'border': 2,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow'})

    for c in range(1, 2 + 6 * W):
        if (c - 1) % 6 == 0 and (c - 1) != 0:
            worksheet.merge_range(0, c, df.shape[0], c, "", empty_format)
        elif (c - 1) % 6 != 0 or (c - 1) == 0:
            for r in range(df.shape[0]):
                if "grijs" in df.iloc[r, 1] or "grijs2" in df.iloc[r, 1] or "zorg" in df.iloc[r, 1]:
                    worksheet.write(r + 1, c, df.iloc[r, c], grijs_format)
                elif "groen" in df.iloc[r, 1] or "GFE" in df.iloc[r, 1]:
                    worksheet.write(r + 1, c, df.iloc[r, c], groen_format)
                elif "VEC" in df.iloc[r, 1]:
                    worksheet.write(r + 1, c, df.iloc[r, c], VEC_format)
                elif "papier" in df.iloc[r, 1]:
                    worksheet.write(r + 1, c, df.iloc[r, c], papier_format)

    for i in range(len(gemeente)):
        index_list = df.loc[df["Gemeente"] == gemeente[i]].index.values
        worksheet.merge_range(index_list[0] + 1, 0, index_list[-1] + 1, 0, gemeente[i], gemeente_format)
        # worksheet.merge_range(J*K+1, 0, 2*J*K, 0, "Hof van Twente", gemeente_format)

    writer.save()


def write_route(df, n, A, L, W, K, gemeente, filename):
    # Define excel writer
    writer = pd.ExcelWriter(filename, engine="xlsxwriter")
    df.to_excel(writer, sheet_name="Route schedule", index=False)
    worksheet = writer.sheets["Route schedule"]
    workbook = writer.book

    # Whole lot of formatting stuff to get the Excel sheet to look nice
    worksheet.set_column('A:A', 12)
    worksheet.set_column('B:B', 20)
    worksheet.set_column('C:C', 12)
    worksheet.set_column('D:D', 40)

    # Formats
    gemeente_format = workbook.add_format({
        'bold': 1,
        'font_color': 'red',
        'border': 2,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#F8CBAD'})

    waste_formats = [workbook.add_format({'border': 1, 'fg_color': '#BFBFBF'}),
                     workbook.add_format({'border': 1, 'fg_color': '#92D050'}),
                     workbook.add_format({'border': 1, 'fg_color': '#FFC000'}),
                     workbook.add_format({'border': 1, 'fg_color': '#00B0F0'})]

    empty_format = workbook.add_format({
        'border': 2,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow'})

    for c in range(1, 4 + 6 * W):
        # Set first two column to correct colors
        r = 0
        for i in range(n):
            for l in range(L):
                for j in range(A):
                    for k in range(K[i, l, j]):
                        if c == 1 or c == 2:
                            worksheet.write(r + 1, c, df.iloc[r, c], waste_formats[j])
                        elif (c - 3) % 6 != 0:
                            if df.iloc[r, c] == 1:
                                worksheet.write(r + 1, c, "x", waste_formats[j])
                        r += 1

        if (c - 3) % 6 == 0 and (c - 3) != 0:
            worksheet.merge_range(0, c, r, c, "", empty_format)
        # elif (c - 2) % 6 != 0:
        #     for r in range(n * A * L):
        #         if r % 4 == 0:
        #             worksheet.write(r + 1, c, df.iloc[r, c], waste_formats[0])
        #         elif r % 4 == 1:
        #             worksheet.write(r + 1, c, df.iloc[r, c], waste_formats[1])
        #         elif r % 4 == 2:
        #             worksheet.write(r + 1, c, df.iloc[r, c], waste_formats[2])
        #         elif r % 4 == 3:
        #             worksheet.write(r + 1, c, df.iloc[r, c], waste_formats[3])

    prev_row = 0
    for i in range(n):
        K_tot = np.sum(K[i, :, :])
        worksheet.merge_range(prev_row+1, 0, prev_row+K_tot, 0, gemeente[i], gemeente_format)
        prev_row += K_tot

    writer.save()
