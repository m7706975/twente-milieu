import json
import numpy as np
from math import cos, sin
import pandas as pd
from create_graph import create_graph, graph_adjust_almelo
from match_graph import match_graph
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import networkx as nx
from matplotlib import pyplot as plt, animation
from match_route_address import haversine
import time


def load_graph(route):
    # Load road network graph
    with open('municipality_graphs/almelo_graph2.json', 'r') as f:
        data = json.load(f)

    # Create graph from file
    nodes = data["nodes"]
    edges = data["edges"]
    restrictions = data["restrictions"]
    print("Read data")

    # Make the graph
    nodes, edges = graph_adjust_almelo(nodes, edges)
    G, ref_dict, reverse_ref_dict = create_graph(nodes, edges)
    print("Made graph")

    tic = time.time()
    # Match graph to addresses
    # df = pd.read_excel("almelo_match_loc_final.xlsx")
    # match_graph(df, G, True)
    # nx.write_gpickle(G, "matched_graph2.gpickle")
    G = nx.read_gpickle("matched_graph2.gpickle")

    toc = time.time()
    print(f"Time to match graph: {toc-tic:.{1}f}s")
    print("Matched graph")

    # Select all nodes and edges in the area of route
    with open('route_polygons/' + route + '.json', 'r') as f:
        data_polygon = json.load(f)
    data_tuples = [tuple(x) for x in data_polygon["coordinates"][0]]
    polygon = Polygon(data_tuples)

    # Make and return route graph
    G_route = route_graph(G.copy(), route_full, polygon, ref_dict)
    print("Made subgraph")

    nx.set_edge_attributes(G_route, 0, "garbage_direction")

    # Write graph data of route
    # with open('TM_adjust_graphs/' + route + '_graph.json', 'w') as outfile:
    #     gtsp_data = {"nodes": list(G_route.nodes(data=True)), "edges": list(G_route.edges(data=True))}
    #     json.dump(gtsp_data, outfile)

    # Load data of route
    with open('TM_adjust_graphs/' + route + '_adjust_graph.json', 'r') as outfile:
        data = json.load(outfile)
        G_route = nx.Graph()
        G_route.add_nodes_from(data["nodes"])
        G_route.add_edges_from(data["edges"])

    return G_route


def route_graph(graph, route, polygon, ref_dict):
    # Check which nodes are in the route polygon
    route_h = route.replace(" ", "")
    if route_h == "ALZL08BRWO":
        with open('route_polygons/' + route_h + 'not.json', 'r') as f:
            data_polygon2 = json.load(f)
        data_tuples2 = [tuple(x) for x in data_polygon2["coordinates"][0]]
        polygon2 = Polygon(data_tuples2)

    nodes_to_remove = []
    for n in graph.nodes(data=True):
        if not polygon.contains(Point(n[1]['loc'][0], n[1]['loc'][1])):
            nodes_to_remove.append(n[0])
        elif route_h == "ALZL08BRWO":
            if polygon2.contains(Point(n[1]['loc'][0], n[1]['loc'][1])):
                nodes_to_remove.append(n[0])

    # Remove nodes not in route
    graph.remove_nodes_from(nodes_to_remove)

    # Only keep the large connected component
    components = [graph.subgraph(p).copy() for p in nx.connected_components(graph)]
    large_g = max([len(g) for g in components])
    for g in components:
        if len(g) < large_g:
            for n in g.nodes():
                graph.remove_node(n)

    # print(len([graph.subgraph(p).copy() for p in nx.connected_components(graph)]))
    # print(len(graph.nodes()))

    # Add per edge the weight (haversine distance between nodes), starting node and if it is a required edge
    att_list = ['ref', 'nodes', 'road', 'urban', 'restroute', 'gftroute', 'grijs', 'groen', 'VEC', 'papier']
    for e in graph.edges(data=True):
        lon1, lat1 = graph.nodes[e[0]]['loc']
        lon2, lat2 = graph.nodes[e[1]]['loc']
        # Right now weight is just distance, can divide by driving speed
        graph[e[0]][e[1]]['weight'] = haversine(lon1, lat1, lon2, lat2)
        graph[e[0]][e[1]]['start'] = ref_dict[e[2]['start']]
        if e[2]['restroute'] == route:
            graph[e[0]][e[1]]['req'] = 1
        elif route == "AL AL15 DRWO" and e[2]['restroute'] == "AL AL15 DRWO (50%)":
            graph[e[0]][e[1]]['req'] = 1
        else:
            graph[e[0]][e[1]]['req'] = 0
        # Remove attributes that we don't need
        for att in att_list:
            e[2].pop(att, None)

    return graph


def make_GTSP(graph):
    cluster_count = 0
    node_count = 0
    # clusters is a dictionary with the cluster as key and nodes (corresponding to required edges) as values
    clusters = {}
    # node_to_edge gives per node in the GTSP the edge in the original graph
    node_to_edge = {}
    shortest_paths = {}

    # Perform dijkstra on every required edge and make clusters
    for e in graph.edges(data=True):
        if 'garbage_direction' not in e[2].keys():
            e[2]['garbage_direction'] = 0

        # Check what is the starting node
        if e[0] == e[2]["start"]:
            node1 = e[0]
            node2 = e[1]
        else:
            node1 = e[1]
            node2 = e[0]
        # Add clusters and nodes to GTSP
        if e[2]['req'] == 1:
            # print(cluster_count)
            cluster_count += 1
            # Check if oneway
            if e[2]['oneway'] == 1 or e[2]['garbage_direction'] == 1:
                # For one-way street we add one cluster with one node
                node_count += 1
                clusters[cluster_count] = [node_count]
                # Perform dijkstra for this one direction
                dist, prev = dijkstra(graph, node2)
                shortest_paths[(node1, node2)] = {'dist': dist, 'prev': prev}
                # Link node in GTSP to edge
                node_to_edge[node_count] = (node1, node2)
            elif e[2]['garbage_direction'] == 2:
                # For streets we need to pass both ways we simply add two clusters of size 1
                node_count += 2
                clusters[cluster_count] = [node_count - 1]
                cluster_count += 1
                clusters[cluster_count] = [node_count]
                # Perform dijkstra for both directions
                dist, prev = dijkstra(graph, node2)
                shortest_paths[(node1, node2)] = {'dist': dist, 'prev': prev}
                dist, prev = dijkstra(graph, node1)
                shortest_paths[(node2, node1)] = {'dist': dist, 'prev': prev}
                # Link node in GTSP to edges
                node_to_edge[node_count - 1] = (node1, node2)
                node_to_edge[node_count] = (node2, node1)
            else:
                # For two-way streets we add one cluster with two nodes
                node_count += 2
                clusters[cluster_count] = [node_count - 1, node_count]
                # Perform dijkstra for both directions
                dist, prev = dijkstra(graph, node2)
                shortest_paths[(node1, node2)] = {'dist': dist, 'prev': prev}
                dist, prev = dijkstra(graph, node1)
                shortest_paths[(node2, node1)] = {'dist': dist, 'prev': prev}
                # Link node in GTSP to edges
                node_to_edge[node_count - 1] = (node1, node2)
                node_to_edge[node_count] = (node2, node1)

    # Here we add a final node to the GTSP that will have 0 distance to all other nodes. This ensures that we don't need
    # a cycle, but can start and end in any node.
    node_count += 1
    cluster_count += 1
    clusters[cluster_count] = [node_count]

    # Dummy node will have distance from starting point to all other nodes as weights
    min_dict = {"ALAL01DRVR": 22996, "ALAL02ARVR": 22996, "ALAL03DRWO": 22784, "ALAL04BRWO": 9920, "ALAL05CRWO": 4602,
                "ALAL06ARDO": 3228, "ALAL07DRDO": 6791, "ALAL08DRVR": 6857, "ALAL09ARWO": 6961, "ALAL10ARMA": 16542,
                "ALAL11DRMA": 16542, "ALAL12DRMA": 6099, "ALAL13DRDI": 6101, "ALAL14DRDI": 8316, "ALAL15DRWO": 9025,
                "ALAL23ARDI": 2278, "ALZL04BRDI": 3441, "ALZL06DRDI": 17901, "ALZL13CRVR": 4088, "ALZL12BRMA": 19045,
                "ALZL16DRMA": 11715,
                "ALZL08BRWO": 14206, "ALZL05CRDO": 2345, "ALZL17BRVR": 9375, "ALZL07BRDO": 9660,
                "ALZL01DRWO": 1584, "ALZL10ARDI": 2022}
    start_node = min_dict[route]
    dist_start, prev = dijkstra(graph, start_node)

    # Make the edge weight matrix
    C = np.zeros((node_count, node_count))
    for i in range(node_count-1):
        # Get edge and shortest paths for this node (edge in original graph)
        edge1 = node_to_edge[i+1]
        short_dict = shortest_paths[edge1]
        dist = short_dict['dist']
        prev = short_dict['prev']
        # Make transition matrix
        for j in range(node_count-1):
            if i == j:
                C[i, j] = 999999
            else:
                edge2 = node_to_edge[j+1]
                C[i, j] = round(10000*(dist[edge2[0]] + graph[edge2[0]][edge2[1]]["weight"]))
        # Add final column
        C[i, node_count-1] = 0
    # Add final row for empty node
    for j in range(node_count-1):
        edge2 = node_to_edge[j + 1]
        C[node_count-1, j] = round(10000*(dist_start[edge2[0]] + graph[edge2[0]][edge2[1]]["weight"]))
    C[node_count-1, node_count-1] = 999999

    C.astype(int)

    return cluster_count, node_count, clusters, node_to_edge, shortest_paths, C


def get_gtour_edges(shortest_paths, node_to_edge):
    # Obtain the edges in the gtour
    edges_in_tour = []
    prev_edge = node_to_edge[node_path[0]]
    edges_in_tour.append(prev_edge)
    for n in range(1, len(node_path)):
        curr_edge = node_to_edge[node_path[n]]
        # If the end node of previous edge is the same as first node of current edge, the path between them is empty
        if curr_edge[0] != prev_edge[1]:
            # Otherwise, we find the path using the shortest_paths prev dict of our prev_edge
            found = False
            prev_dict = shortest_paths[prev_edge]['prev']
            curr_node = curr_edge[0]
            path_list = []
            while not found:
                prev_node = prev_dict[curr_node]
                path_list.append((prev_node, curr_node))
                # If we arrived back at the source node, we have found the entire path
                if prev_node == prev_edge[1]:
                    found = True
                curr_node = prev_node
            edges_in_tour += reversed(path_list)

        # Also append the required edge to the tour
        edges_in_tour.append(curr_edge)
        prev_edge = curr_edge

    # Obtain total distance of tour
    tot_dist = 0
    for n1, n2 in edges_in_tour:
        tot_dist += G_route[n1][n2]["weight"]

    return edges_in_tour, tot_dist


def dijkstra(graph, source):
    # Get the shortest paths from s to all other nodes
    dist = {}
    prev = {}
    queue = []
    for vertex in graph.nodes:
        dist[vertex] = np.inf
        queue.append(vertex)

    dist[source] = 0
    while len(queue) > 0:
        # Get element with min dist, works since all edge weights are positive
        u = min(queue, key=dist.get)
        queue.remove(u)

        # Loop over all neighbours of v that are still in the queue
        for v in graph.neighbors(u):
            # Oneway streets in wrong direction are not considered
            if graph[u][v]['oneway'] == 1:
                if graph[u][v]['start'] == v:
                    continue
            if v in queue:
                new_dist = dist[u] + graph[u][v]['weight']
                # If shorter path found, update to new distance
                if new_dist < dist[v]:
                    dist[v] = new_dist
                    prev[v] = u

    return dist, prev


def write_GTSP_file(name, node_count, cluster_count, clusters, C):
    file = open("TM_GTSP/"+name+".gtsp", "w+")

    file.write("NAME : %s\n" % name)
    file.write("TYPE : AGTSP\n")
    file.write("DIMENSION : %d\n" % node_count)
    file.write("GTSP_SETS : %d\n" % cluster_count)
    file.write("EDGE_WEIGHT_TYPE : EXPLICIT\n")
    file.write("EDGE_WEIGHT_FORMAT : FULL_MATRIX\n")
    file.write("EDGE_WEIGHT_SECTION\n")

    for i in range(node_count):
        for j in range(node_count):
            file.write("%d " % C[i][j])
        file.write("\n")

    file.write("GTSP_SET_SECTION\n")
    for i in range(1, cluster_count+1):
        file.write("%d " % i)
        for j in clusters[i]:
            file.write("%d " % j)
        file.write("-1\n")
    file.write("EOF\n")

    file.close()


def write_param_file(name):
    # Write the parameter file
    file = open("TM_PAR/" + name + ".par", "w+")

    file.write("PROBLEM_FILE = TM_GTSP/%s.gtsp\n" % name)
    file.write("ASCENT_CANDIDATES = %s\n" % 500)
    file.write("INITIAL_PERIOD = %s\n" % 1000)
    file.write("MAX_CANDIDATES = %s\n" % 12)
    file.write("MAX_TRIALS = %s\n" % 1000)
    file.write("MOVE_TYPE = %s\n" % 5)
    file.write("OUTPUT_TOUR_FILE = TM_G-TOURS/%s.tour\n" % name)
    file.write("PI_FILE = TM_PI_FILES/%s.pi\n" % name)
    file.write("RUNS = %s\n" % 1)
    file.write("SEED = %s\n" % 1)
    file.write("TRACE_LEVEL = %s\n" % 0)

    file.close()


def read_TOUR_file(name):
    file = open("TM_TOURS/"+name+"2.tour", 'r')
    lines = file.readlines()

    opt = 0
    read_nodes = False
    node_path = []
    for line in lines:
        # print(line, type(line), len(line), line.isdigit())
        if "COMMENT : Length" in line:
            opt = [int(s) for s in line.split() if s.isdigit()][0]
        elif "TOUR_SECTION" in line:
            read_nodes = True
        elif "-1" in line:
            read_nodes = False
        elif read_nodes:
            # print([int(s) for s in line.split() if s.isdigit()])
            node_path.append([int(s) for s in line.split() if s.isdigit()][0])

    # Remove extra node and shift list
    dummy_idx = node_path.index(max(node_path))
    node_path = node_path[dummy_idx+1:] + node_path[:dummy_idx]

    return opt, node_path


def animate(i):
    # Draw next edge
    global draw_edge
    if i == 0:
        draw_edge = (0, 0)
    edge = edges_in_tour[i]
    if edge != draw_edge:
        G_anim.clear()
        if draw_edge == (0, 0):
            # In the first frame, we only draw th first edge in the path
            G_anim.add_nodes_from([1, 2])
            G_anim.add_edge(1, 2)
            pos = {1: node_coords[edge[0]], 2: node_coords[edge[1]]}
            nx.draw_networkx_edges(G_anim, pos, [(1, 2)], node_size=3, edge_color='r', width=2)
        else:
            # In all subsequent frames, we draw the first edge red and all previous green
            G_anim.add_nodes_from([1, 2, 3])
            G_anim.add_edges_from([(1, 2), (2, 3)])
            pos = {1: node_coords[draw_edge[0]], 2: node_coords[draw_edge[1]], 3: node_coords[edge[1]]}
            nx.draw_networkx_edges(G_anim, pos, [(1, 2)], node_size=3, edge_color='orange', width=2)
            nx.draw_networkx_edges(G_anim, pos, [(2, 3)], node_size=3, edge_color='r', width=2)

    # Store edge, so we don't draw it again in the next fram if its the same one
    draw_edge = edge


def animated_tour(edges_in_tour):
    # Draw initial base figure
    fig = plt.figure()
    nx.draw(G_route, node_coords, node_size=3, node_color='blue', edge_color='black')

    # Time per frame
    t = []
    for edge in edges_in_tour:
        t.append(round(300*G_route[edge[0]][edge[1]]['weight']))

    # Create index list for frames, i.e. how many cycles each frame will be displayed
    frame_t = []
    for i, item in enumerate(t):
        frame_t.extend([i] * item)

    # Graph for animation
    global G_anim
    G_anim = nx.Graph()

    # Animate plot
    anim = animation.FuncAnimation(fig, animate, frames=frame_t, interval=1, repeat=False)
    plt.show()


if __name__ == '__main__':
    # Define the route
    route_full = "AL AL08 DRVR"
    route = route_full.replace(" ", "")
    G_route = load_graph(route)

    # Make the GTSP
    tic = time.time()
    cluster_count, node_count, clusters, node_to_edge, shortest_paths, edge_weight_matrix = make_GTSP(G_route)
    toc = time.time()
    print(f"Elapsed time to make GTSP: {toc-tic:.{1}f}s")
    print("Made GTSP")

    # Make GTSP and parameter file
    # write_GTSP_file(route, node_count, cluster_count, clusters, edge_weight_matrix)
    # write_param_file(route)
    # print("Wrote to file")

    # Write or load shortest paths and node-edge relations from GTSP, used to make routes
    # Write data
    # with open('TM_json/' + route + '_gtsp.json', 'w') as outfile:
    #     gtsp_data = {"shortest_paths": shortest_paths, "node_to_edge": node_to_edge}
    #     json.dump(gtsp_data, outfile)

    # Load data
    # with open('TM_json/' + route + '_gtsp.json', 'r') as outfile:
    #     data = json.load(outfile)
    #     shortest_paths, node_to_edge = data["shortest_paths"], data["node_to_edge"]

    # Read Tour file
    opt, node_path = read_TOUR_file(route)
    edges_in_tour, tot_dist = get_gtour_edges(shortest_paths, node_to_edge)
    print("Read tour file, length equals: ", opt)

    # Dummy node will have distance from starting point to all other nodes as weights
    min_dict = {"ALAL01DRVR": 22996, "ALAL02ARVR": 22996, "ALAL03DRWO": 22784, "ALAL04BRWO": 9920, "ALAL05CRWO": 4602,
                "ALAL06ARDO": 3228, "ALAL07DRDO": 6791, "ALAL08DRVR": 6791, "ALAL09ARWO": 6961, "ALAL10ARMA": 16542,
                "ALAL11DRMA": 16542, "ALAL12DRMA": 6099, "ALAL13DRDI": 6101, "ALAL14DRDI": 8316, "ALAL15DRWO": 9025,
                "ALAL23ARDI": 2278, "ALZL04BRDI": 3441, "ALZL06DRDI": 17901, "ALZL13CRVR": 4088, "ALZL12BRMA": 19045,
                "ALZL16DRMA": 11715,
                "ALZL08BRWO": 14206, "ALZL05CRDO": 2345, "ALZL17BRVR": 9375, "ALZL07BRDO": 9660,
                "ALZL01DRWO": 1584, "ALZL10ARDI": 2022}
    node_test = min_dict[route]
    # node_test = 0
    dist_test, prev_test = dijkstra(G_route, node_test)

    # Plot graph
    node_coords = {}
    node_colors = []
    for n in G_route.nodes(data=True):
        node_coords[n[0]] = n[1]["loc"]
        if n[0] == node_test:
            node_colors.append('green')
        elif n[0] == 16948:
            node_colors.append('pink')
        elif n[0] == 16949:
            node_colors.append('purple')
        elif dist_test[n[0]] == np.inf:
            node_colors.append('red')
        else:
            node_colors.append('blue')

    edge_colors = []
    # Triangle properties
    a_triangle = 0.00005
    R_triangle = a_triangle*(np.sqrt(3)/3)
    theta1, theta2 = 2*np.pi/3, 4*np.pi/3
    rot120 = np.array([[cos(theta1), -sin(theta1)], [sin(theta1), cos(theta1)]])
    rot240 = np.array([[cos(theta2), -sin(theta2)], [sin(theta2), cos(theta2)]])

    for e in G_route.edges(data=True):
        # if (e[0], e[1]) in weird_edges or (e[1], e[0]) in weird_edges:
        #     edge_colors.append('red')
        #     print(e[2]["start"])

        # Draw direction vectors for oneway street
        if e[2]["oneway"] or e[2]['garbage_direction'] == 1:
            # Get nodes and their locations
            start = e[2]["start"]
            if e[0] == start:
                node1 = np.array(G_route.nodes[e[0]]["loc"])
                node2 = np.array(G_route.nodes[e[1]]["loc"])
            else:
                node1 = np.array(G_route.nodes[e[1]]["loc"])
                node2 = np.array(G_route.nodes[e[0]]["loc"])
            # Make vector in direction of street
            vec = node2-node1
            vec_norm = vec/np.linalg.norm(vec)
            mid = node1/2+node2/2
            vec_rot120 = np.dot(rot120, vec_norm)
            vec_rot240 = np.dot(rot240, vec_norm)

            # Make and draw triangle
            mid2 = np.array([mid+R_triangle*vec_norm, mid+R_triangle*vec_rot120, mid+R_triangle*vec_rot240])

            plt.scatter(mid[0], mid[1], s=0, color='green')
            plt.scatter(mid2[:, 0], mid2[:, 1], s=0, color=['pink', 'brown', 'brown'])
            t1 = plt.Polygon(mid2, color='red')
            plt.gca().add_patch(t1)
            # for i in range(3):
            #     plt.plot([mid[0], mid2[i, 0]], [mid[1], mid2[i, 1]], 'k-')

        if e[2]["req"] == 1:
            if e[2]["garbage_direction"] == 1:
                edge_colors.append('red')
            elif e[2]["garbage_direction"] == 2:
                edge_colors.append('green')
            else:
                edge_colors.append('orange')
        else:
            edge_colors.append('black')

    nx.draw(G_route, node_coords, node_size=3, node_color=node_colors, edge_color=edge_colors)
    # plt.show()

    # prev_street = ""
    # for (node1, node2) in edges_in_tour:
    #     street = G_route[node1][node2]["name"]
    #     if street != prev_street:
    #         print(street)
    #     prev_street = street

    # Animate function
    print(f"Length optimum path: {tot_dist:.{2}f} km")
    animated_tour(edges_in_tour)


