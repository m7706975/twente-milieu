import osmium as o
import pandas as pd
import json
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import difflib
import time
import re


class AddressHandler(o.SimpleHandler):
    def __init__(self, streets, exact):
        super(AddressHandler, self).__init__()
        self.address = []
        self.total = 0
        self.found_streets = []
        self.streets = streets
        self.exact = exact

    def node(self, n):
        # Finds all nodes in polygon that have street and housenumber (so these nodes are a house). Every node whose
        # street name is in input list self.streets is stored in self.address. We also store for which streets we have
        # found nodes in self.found_streets.
        if n.location.lat <= max_lat and n.location.lat >+ min_lat and n.location.lon <= max_lon and n.location.lon >= min_lon\
                and "addr:street" in n.tags and "addr:housenumber" in n.tags:
            point = Point(n.location.lon, n.location.lat)
            if polygon.contains(point):
                # self.exact determines if we want exactly matching street name, sometimes small differences in name
                if self.exact:
                    # If match with street, append location to list
                    if n.tags["addr:street"] in self.streets:
                        # Hard coded for sint josephstraat, some nodes have abbreviation st., others not
                        if n.tags["addr:street"] == "Sint Josephstraat":
                            self.address.append({"Street": n.tags["addr:street"], "Street2": "St. Josephstraat",
                                                 "Housenumber": n.tags["addr:housenumber"],
                                                 "lon": n.location.lon, "lat": n.location.lat})
                        else:
                            self.address.append({"Street": n.tags["addr:street"], "Street2": n.tags["addr:street"],
                                                 "Housenumber": n.tags["addr:housenumber"],
                                                 "lon": n.location.lon, "lat": n.location.lat})
                        if n.tags["addr:street"] not in self.found_streets:
                            self.found_streets.append(n.tags["addr:street"])
                        self.total += 1
                else:
                    # inexact matching using difflib, match_ratio determines closeness of street names
                    for s in self.streets:
                        match_ratio = difflib.SequenceMatcher(None, s, n.tags["addr:street"]).ratio()
                        # If match with street, append location to list
                        if match_ratio > 0.8:
                            self.address.append({"Street": n.tags["addr:street"], "Street2": s,
                                                 "Housenumber": n.tags["addr:housenumber"],
                                                 "lon": n.location.lon, "lat": n.location.lat})
                            if n.tags["addr:street"] not in self.found_streets:
                                self.found_streets.append(s)
                            self.total += 1


def match_adress(data, addresses):
    # Inputs:
    # - Dataframe with all addresses (data, pandas.Dataframe)
    # - Nodes who match the street names of the Excel file (addresses, list of dictionaries)
    data = data.astype({'Huisnummer': 'str'})
    data_dict = data.to_dict('records')

    count = 0
    new_dict_list = []
    # Loop over all addresses of Excel file
    for d1 in data_dict:
        match = False
        # Check for an Huisletter, only keep single letters
        if d1["Huisletter"].isalpha():
            attachment = True
        else:
            attachment = False
        for d2 in addresses:
            # Get housenumber, remove attachment if not a digit
            if d2["Housenumber"].isdigit():
                house_number = d2["Housenumber"]
                attachment2 = False
            # We also set second bool attachment to True if not digit (i.e. there is some attachment)
            else:
                split = re.split('(\d+)', d2["Housenumber"], maxsplit=1)
                house_number = split[1]
                attachment2 = True

            # Check for match, break if exact match found
            if d1["Straat_Naam"] == d2["Street2"] and d1["Huisnummer"] == house_number:
                if attachment:
                    # Does current node also have an attachment
                    if attachment2:
                        # Check if attachments (huisletters) are equal
                        if d1["Huisletter"].lower() == split[2].lower():
                            d1.update(d2)
                            match = True
                            break
                    # If not exact match we also update, but we don't break out of for loop. Thus later iterations can
                    # override dictionary values in d1.
                    d1.update(d2)
                    match = True

                # If the address does not have a huisletter we are instantly done. We update and go to next address
                else:
                    d1.update(d2)
                    match = True
                    break
        if match:
            count += 1
        # For all non matches we try to match it to a nearby address
        if not match:
            match2 = False
            prev_diff = 10
            for d2 in addresses:
                # Check if we have matches in the same street
                if d1["Straat_Naam"] == d2["Street2"]:
                    # Get house number number
                    if d2["Housenumber"].isdigit():
                        house_number = d2["Housenumber"]
                    else:
                        split = re.split('(\d+)', d2["Housenumber"], maxsplit=1)
                        house_number = split[1]
                    # Difference in house number, if smaller than previous store as new closest address. Must be at
                    # least within 10 housenumbers (we could make it care about even/odd).
                    diff = abs(int(d1["Huisnummer"]) - int(house_number))
                    if diff < prev_diff:
                        close_address = d2
                        prev_diff = diff
                        match2 = True
            if match2:
                d1.update(close_address)
                count += 1
            else:
                d1.update({"Street": "", "Street2": "", "Housenumber": "", "lon": 0, "lat": 0})
                print(d1["Straat_Naam"], d1["Huisnummer"])
        new_dict_list.append(d1)
        if count % 1000 == 0:
            print(count)
            print("Elapsed time:", time.time()-tic)

    print(len(data_dict)-count)
    new_df = pd.DataFrame(new_dict_list)

    match_route(new_df)
    # new_df.to_excel("almelo_match_loc.xlsx")


def match_route(df_match):
    df1 = pd.read_csv("csv_files/Containers_-1_Almelo_Rest_Achterlader.csv", sep=";")
    df2 = pd.read_csv("csv_files/Containers_-1_Almelo_Rest_Zijlader.csv", sep=";")
    df3 = pd.read_csv("csv_files/Containers_-1_Almelo_GFT_Achterlader.csv", sep=";")
    df4 = pd.read_csv("csv_files/Containers_-1_Almelo_GFT_Zijlader.csv", sep=";")

    # Change postcodes in df_match to remove the space
    for index, row in df_match.iterrows():
        split = row["Postcode"].split(' ')
        df_match.iloc[index, 1] = split[0]+split[1]

    # For df2 we need to make sure some entries are integers
    idx = ~df2['huisnummer'].str.isdigit()
    for i in range(len(idx)):
        if idx[i]:
            df2.iloc[i, 3] = df2.iloc[i, 3].split(',')[0]

    idx = ~df4['huisnummer'].str.isdigit()
    for i in range(len(idx)):
        if idx[i]:
            df4.iloc[i, 3] = df4.iloc[i, 3].split(',')[0]

    # Transform dataframe types
    df2 = df2.astype({'huisnummer': 'int64'})
    df4 = df4.astype({'huisnummer': 'int64'})
    df_match = df_match.astype({'Huisnummer': 'int64'})

    # Merge, first with df1, then with df2
    df_merge = df_match.merge(df1[["postcode", "straat", "huisnummer", "gebiednaam"]].drop_duplicates(),
                              left_on=["Postcode", "Straat_Naam", "Huisnummer"], right_on=["postcode", "straat", "huisnummer"], how='left')
    df_merge.drop(["postcode", "straat", "huisnummer"], axis=1, inplace=True)
    df_merge.rename(columns={"gebiednaam": "restroute_a"}, inplace=True)

    df_merge = df_merge.merge(df2[["postcode", "straat", "huisnummer", "gebiednaam"]].drop_duplicates(),
                              left_on=["Postcode", "Straat_Naam", "Huisnummer"], right_on=["postcode", "straat", "huisnummer"], how='left')
    df_merge.drop(["postcode", "straat", "huisnummer"], axis=1, inplace=True)
    df_merge.rename(columns={"gebiednaam": "restroute_z"}, inplace=True)

    # Combine restroute columns
    rest_row = []
    for index, row in df_merge.iterrows():
        if isinstance(row['restroute_a'], str):
            rest_row.append(row['restroute_a'])
        elif isinstance(row['restroute_z'], str):
            rest_row.append(row['restroute_z'])
        else:
            rest_row.append("")

    df_merge["restroute"] = rest_row
    df_merge.drop(["restroute_a", "restroute_z"], axis=1, inplace=True)

    # Now we repeat this process for GFT routes

    # Merge, first with df1, then with df2
    df_merge = df_merge.merge(df3[["postcode", "straat", "huisnummer", "gebiednaam"]].drop_duplicates(),
                              left_on=["Postcode", "Straat_Naam", "Huisnummer"],
                              right_on=["postcode", "straat", "huisnummer"], how='left')
    df_merge.drop(["postcode", "straat", "huisnummer"], axis=1, inplace=True)
    df_merge.rename(columns={"gebiednaam": "gftroute_a"}, inplace=True)

    df_merge = df_merge.merge(df4[["postcode", "straat", "huisnummer", "gebiednaam"]].drop_duplicates(),
                              left_on=["Postcode", "Straat_Naam", "Huisnummer"],
                              right_on=["postcode", "straat", "huisnummer"], how='left')
    df_merge.drop(["postcode", "straat", "huisnummer"], axis=1, inplace=True)
    df_merge.rename(columns={"gebiednaam": "gftroute_z"}, inplace=True)

    # Combine restroute columns
    rest_row = []
    for index, row in df_merge.iterrows():
        if isinstance(row['gftroute_a'], str):
            rest_row.append(row['gftroute_a'])
        elif isinstance(row['gftroute_z'], str):
            rest_row.append(row['gftroute_z'])
        else:
            rest_row.append("")

    df_merge["gftroute"] = rest_row
    df_merge.drop(["gftroute_a", "gftroute_z"], axis=1, inplace=True)

    df_merge.to_excel("almelo_test.xlsx")


if __name__ == '__main__':

    tic = time.time()

    # Open polygon shape of current municipality or route region
    municipality = 'almelo'
    with open('municipality_polygon/' + municipality + '_shape.json', 'r') as f:
        data = json.load(f)
    # Store coordinates and make polygon
    data_tuples = [tuple(x) for x in data["coordinates"][0][0]]
    polygon = Polygon(data_tuples)
    # Get bounding box for the polygon
    max_lon, max_lat = map(max, zip(*data_tuples))
    min_lon, min_lat = map(min, zip(*data_tuples))

    # Load Excel file with addresses and get unique street names
    df = pd.read_excel("05899-minibezitters Almelo_copy.xlsx")
    column_values = df["Straat_Naam"].values.ravel()
    unique_values = list(pd.unique(column_values))
    unique_values += ['Sint Josephstraat']

    # Obtain data on the road network
    # Match addresses from OSM data a street in Excel data exaclty
    h1 = AddressHandler(unique_values, True)
    h1.apply_file("overijssel-latest.osm.pbf", locations=True)

    # Take remaining streets in Excel data that have not been matched
    remaining_streets = list(set(unique_values) - set(h1.found_streets))
    print(remaining_streets)
    print("Elapsed time", time.time()-tic)

    # Inexact matching of streets in OSM data to streets in Excel sheet, accounting for differences such as capitilization, abbreviations, etc.
    h2 = AddressHandler(remaining_streets, False)
    h2.apply_file("overijssel-latest.osm.pbf", locations=True)

    remaining_streets = list(set(remaining_streets) - set(h2.found_streets))
    print("Elapsed time", time.time()-tic)

    # We finally attach the found coordinates to
    address_list = h1.address + h2.address
    match_adress(df, address_list)
    print("Elapsed time", time.time()-tic)



    # addresses, streets = main("overijssel-latest.osm.pbf")
    # print(list(set(unique_values)-set(streets)))
    # print(list(set(streets)-set(unique_values)))

