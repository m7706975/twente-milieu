import pandas as pd
import json
from create_graph import create_graph, graph_adjust_almelo
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import difflib
import osmium as o
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.io.img_tiles import OSM


class StreetHandler(o.SimpleHandler):
    # ---------------- OLD FOR CHECKING MISSING STREETS ---------------------
    # h = StreetHandler(missing1)
    # h.apply_file("overijssel-latest.osm.pbf", locations=True)
    # print(len(h.new_streets), h.new_streets)
    # print(list(set(missing1)-set(h.new_streets)))

    def __init__(self, streets):
        super(StreetHandler, self).__init__()
        self.streets = streets
        self.new_streets = []

    def way(self, w):
        if 'highway' in w.tags:
            cond = False
            for n in w.nodes:
                point = Point(n.location.lon, n.location.lat)
                if polygon.contains(point):
                    cond = True
            if cond and 'name' in w.tags:
                if w.tags['name'] in self.streets:
                    if w.tags['name'] not in self.new_streets:
                        self.new_streets.append(w.tags['name'])


def street_per_route(route):
    df1 = pd.read_csv("csv_files/Containers_-1_Almelo_Rest_Achterlader.csv", sep=';')
    df1 = df1[df1["gebiednaam"] == route]
    column_values = df1["straat"].dropna().values.ravel()
    return pd.unique(column_values)


def min_dist_edge(point, v, w):
    l2 = np.dot(v-w, v-w)
    if l2 == 0:
        return np.linalg.norm(point - v)
    else:
        t = max(0, min(1, np.dot(point - v, w - v) / l2))
        projection = v + t * (w - v)
        return np.linalg.norm(point - projection)


def match_street(df, G, naam):
    # Finds all streets that are both in the dataframe and graph

    column_values = df[naam].dropna().values.ravel()
    unique_values = pd.unique(column_values)

    # Check for matching streets
    streets_found = []
    for e in G.edges:
        edge_data = G.get_edge_data(*e)
        if edge_data["name"] in unique_values and edge_data["name"] not in streets_found:
            streets_found.append(edge_data["name"])

    # Streetnames the closely match, for example "Sint ..." vs "St. ..."

    # diff_set = list(set(unique_values) - set(streets_found))
    # for street in diff_set:
    #     for e in G.edges:
    #         edge_data = G.get_edge_data(*e)
    #         street_name = edge_data["name"]
    #         match_ratio = difflib.SequenceMatcher(None, street, street_name).ratio()
    #         if match_ratio > 0.7:
    #             print(street, street_name, match_ratio)
    #             break
    return unique_values, streets_found


def match_graph(df, G, closest=False):
    # Columns
    df["edge_lon"] = np.NaN
    df["edge_lat"] = np.NaN
    # First group the edges by street name in a dictionary
    street_edge_dict = {}
    for e in G.edges:
        edge_data = G.get_edge_data(*e)
        street_name = edge_data["name"]
        if street_name in street_edge_dict.keys():
            street_edge_dict[street_name].append(e)
        elif street_name != '':
            street_edge_dict[street_name] = [e]

    # Keep all addresses with a matched location
    df1 = df.dropna(subset=['Street'])
    # For each address, find the matching street from dictionary
    edge_match = {}
    for index, row in df1.iterrows():
        match = False
        # Some manual adjustments for containers that are linked to streets very far away
        windmolenbroeksweg = (row['Street'] == "Windmolenbroeksweg" and int(row["Housenumber"]) in [28, 30, 105])
        if row['Street'] in street_edge_dict.keys() and not windmolenbroeksweg:
            if row['Street'] == "Borg Ekenstein":
                edge_list = street_edge_dict["Borg Verhildersum"]
            else:
                edge_list = street_edge_dict[row['Street']]
            dist = 10
            # Select edge with minimum distance from address
            for e in edge_list:
                p = np.array([row['lon'], row['lat']])
                v1 = np.array(G.nodes[e[0]]['loc'])
                v2 = np.array(G.nodes[e[1]]['loc'])
                if min_dist_edge(p, v1, v2) < dist:
                    close_edge = e
                    dist = min_dist_edge(p, v1, v2)
                    match = True
        elif closest:
            # If there is no edge with the street name in the graph, we instead link the container to the nearest street
            dist = 10
            for e in G.edges:
                p = np.array([row['lon'], row['lat']])
                v1 = np.array(G.nodes[e[0]]['loc'])
                v2 = np.array(G.nodes[e[1]]['loc'])
                if min_dist_edge(p, v1, v2) < dist:
                    close_edge = e
                    dist = min_dist_edge(p, v1, v2)
                    match = True

        if match:
            container = row["AL BIO140":"AL VEC240"].fillna(0)
            # Add container to corresponding edge
            G.edges[close_edge]['grijs'] += container["AL RST140"] + container["AL RST240"]
            G.edges[close_edge]['groen'] += container["AL BIO140"] + container["AL BIO240"] + container["AL GFT140"] + container["AL GFT240"]
            G.edges[close_edge]['VEC'] += container["AL VEC140"] + container["AL VEC240"]
            G.edges[close_edge]['papier'] += container["AL PAP140"] + container["AL PAP240"]

            # Set route names
            G.edges[close_edge]['restroute'] = row["restroute"]
            G.edges[close_edge]['gftroute'] = row["gftroute"]

            # Set middle of edge as the point the container connects to
            df.at[index, "edge_lon"] = (G.nodes[close_edge[0]]['loc'][0] + G.nodes[close_edge[1]]['loc'][0])/2
            df.at[index, "edge_lat"] = (G.nodes[close_edge[0]]['loc'][1] + G.nodes[close_edge[1]]['loc'][1])/2


if __name__ == '__main__':
    # Open polygon shape of current municipality or route region
    municipality = 'almelo'
    with open('municipality_polygon/' + municipality + '_shape.json', 'r') as f:
        data = json.load(f)
    # Store coordinates and make polygon
    data_tuples = [tuple(x) for x in data["coordinates"][0][0]]
    polygon = Polygon(data_tuples)

    # Load dataframe with addresses and location
    df = pd.read_excel("almelo_match_loc_final.xlsx")

    # Load road network graph
    with open('municipality_graphs/almelo_graph.json', 'r') as f:
        data = json.load(f)
    nodes = data["nodes"]
    edges = data["edges"]
    restrictions = data["restrictions"]

    # Make adjustments for missing edges and create graph
    nodes, edges = graph_adjust_almelo(nodes, edges)
    G, ref_dict, reverse_ref_dict = create_graph(nodes, edges)

    # Get streets that have no match
    unique_values1, streets1 = match_street(df, G, "Street")
    missing1 = list(set(unique_values1) - set(streets1))
    # print(len(unique_values1), len(missing1), missing1)

    # # Match addresses to streets on the graph
    # match_graph(df, G, True)
    #
    # # Remove small graph components without required edges of rest
    # components = [G.subgraph(p).copy() for p in nx.connected_components(G)]
    # for g in components:
    #     yes = True
    #     for u, v in g.edges():
    #         if G[u][v]['grijs'] > 0:
    #             yes = False
    #             break
    #     if yes:
    #         for node in g:
    #             G.remove_node(node)
    #
    # # Here we make a dataframe of all addresses with coordinates that have a grijs container
    # df_rest = df.dropna(subset=['Street']).dropna(subset=['AL RST140', 'AL RST240'], how='all')
    # # We make this dataframe into a graph of nodes with geocoordinates
    # nodes_loc = []
    # n_count = 0
    # edge_tuples = []
    # for index, row in df_rest.iterrows():
    #     # Append nodes
    #     nodes_loc.append((n_count, {"loc": [row['lon'], row['lat']], "type": "container"}))
    #     nodes_loc.append((n_count+1, {"loc": [row['edge_lon'], row['edge_lat']], "type": "road"}))
    #     # Append edges
    #     edge_tuples.append((n_count, n_count + 1))
    #     n_count += 2
    #
    # # Graph of addresses with grijs container
    # G_loc = nx.Graph()
    # G_loc.add_nodes_from(nodes_loc)
    # G_loc.add_edges_from(edge_tuples)

    # Write and/or read graphs
    # nx.write_gpickle(G, "matched_graph.gpickle")
    # nx.write_gpickle(G_loc, "container_graph.gpickle")
    G = nx.read_gpickle("matched_graph.gpickle")
    G_loc = nx.read_gpickle("container_graph.gpickle")

    # Get coordinates of nodes in road network graph
    loc = np.array([6.716172, 52.375741])
    min_dist = 10
    min_dist_node = 0
    node_coords = {}
    for n in G.nodes:
        node_coords[n] = nodes[n]["loc"]
        dist = np.linalg.norm(np.array(nodes[n]["loc"]) - loc)
        if dist < min_dist:
            min_dist = dist
            min_dist_node = n
    print(G.nodes(data=True)[min_dist_node])

    # Get coordinates of container graph
    node_coords2 = {}
    node_coloring = []
    node_sizes = []
    for n in G_loc.nodes(data=True):
        node_coords2[n[0]] = n[1]["loc"]
        if n[1]["type"] == "container":
            node_coloring.append('r')
            node_sizes.append(3)
        else:
            node_coloring.append('b')
            node_sizes.append(0)

    # Get nodes and ways with a relation
    # node_refs = []
    # edge_refs = []
    # for restr in restrictions:
    #     for member in restr["members"]:
    #         if member["type"] == "n":
    #             node_refs.append(member["ref"])
    #         if member["type"] == "w":
    #             edge_refs.append(member["ref"])

    # Color edges and widen edges in the selected route
    colors = []
    width = []
    route_name = "AL AL08 DRVR"
    for u, v in G.edges:
        if G[u][v]['restroute'] == route_name:
            colors.append('orange')
            width.append(2)
        else:
            colors.append('black')
            width.append(1)

    req_edges = [1 if G[u][v]['grijs'] > 0 else 0 for u, v in G.edges]

    # Node colors
    node_cols = []
    for node in G.nodes:
        if node == min_dist_node:
            node_cols.append("green")
        elif node == 1471:
            node_cols.append("purple")
        elif node == 1475:
            node_cols.append("purple")
        else:
            node_cols.append("blue")
    # node_cols = 'b'

    # Min dist
    min_dict = {"ALAL01DRVR": [6.671209, 52.378960], "ALAL02ARVR": [6.671209, 52.378960],
                "ALAL03DRWO": [6.652634, 52.373332], "ALAL04BRWO": [6.645172, 52.361529],
                "ALAL05CRWO": [6.660887, 52.368245], "ALAL06ARDO": [6.670778, 52.361944],
                "ALAL07DRDO": [6.675230, 52.348315], "ALAL08DRVR": [6.668579, 52.346490],
                "ALAL09ARWO": [6.660200, 52.350426], "ALAL10ARMA": [6.658903, 52.346131],
                "ALAL11DRMA": [6.658903, 52.346131], "ALAL12DRMA": [6.649990, 52.347562],
                "ALAL13DRDI": [6.650176, 52.347608], "ALAL14DRDI": [6.636462, 52.338481],
                "ALAL15DRWO": [6.637401, 52.338503], "ALAL23ARDI": [6.702005, 52.335621],
                "ALZL04BRDI": [6.671486, 52.362212], "ALZL06DRDI": [6.652164, 52.365998],
                "ALZL13CRVR": [6.671539, 52.367030], "ALZL12BRMA": [6.624986, 52.356556],
                "ALZL16DRMA": [6.629017, 52.339489],
                "ALZL08BRWO": [6.684816, 52.343697], "ALZL17BRVR": [6.640391, 52.330465],
                "ALZL01DRWO": [6.688274, 52.378888], "ALZL05CRDO": [6.684540, 52.364971],
                "ALZL10ARDI": [6.684036, 52.344531], "ALZL07BRDO": [6.651257, 52.360988],
                "schierstins": [6.675725, 52.381316],
                "schierstins2": [6.674515, 52.380260],
                "schierstins3": [6.673681, 52.380219],
                "schierstins4": [6.672866, 52.380130],
                "schierstins5": [6.673244, 52.380195]
                }

    # Find start nodes per route
    for k, v in min_dict.items():
        min_dist = 10
        min_dist_node = 0
        for node in G.nodes(data=True):
            dist = np.linalg.norm(np.array(node[1]["loc"]) - np.array(v))
            if dist < min_dist:
                min_dist = dist
                min_dist_node = node
        print(k, min_dist, min_dist_node)

    points = list(min_dict.values())
    # for point in points:
    #     plt.scatter(point[0], point[1])

    nx.draw(G, node_coords, node_size=3, node_color=node_cols, edge_color=colors, width=width)
    # nx.draw(G_loc, node_coords2, node_size=node_sizes, node_color=node_coloring, width=0.1)
    plt.show()
