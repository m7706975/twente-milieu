# Optimisation of scheduling and routing for household waste collection.

This repository contains the Python code for the optimisation of waste collection from mini containers. The code can be divided into two parts, scheduling and routing.

## Scheduling
The scheduling code consists of 3 files.
- `schedule.py`: This file contains the main  MILP for the optimisation of the cyclic schedule that determines when the mini containers in each waste collection route are collected.
- `visualize_schedule.py`: This file writes the output schedule to an Excel file, in which the number of routes per day for each type are given.
- `input_schedule.py`: This file gives input to the `schedule.py` file.


## Routing
The routing code consists of 5 files.
- `graph_map.py`: This file loads OpenStreetMap data on the road network and saves this data to a `.json` file.
- `create_graph.py`: This file converts the graph data to graph of nodes and vertices. It is used by the other files.
- `match_loc.py`: This file finds the locations for each household that has containers that need to be collected. This is again done using OpenStreetMap data.
- `match_graph.py`: This file matches the container locations to edges of the road network. 
- `transform_GTSP.py`: This file transform the final road netowrk for a specific route to a GTSP, and finds the optimal route with the shortest driving distance while visiting all containers.
