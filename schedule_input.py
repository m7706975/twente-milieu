import numpy as np
import json
from math import gcd

with open('schedule_input.json', 'r') as f:
  route_list = json.load(f)

gemeente = []
laders = []
afval_type = []
for r in route_list:
    if r["gemeente"] not in gemeente:
        gemeente.append(r["gemeente"])
    if r["lader"] not in laders:
        laders.append(r["lader"])
    if r["afval"] not in afval_type:
        afval_type.append(r["afval"])

I = len(gemeente)
J = len(laders)
K = len(afval_type)
afval_type = ["grijs", "grijs2", "zorg", "groen", "GFE", "VEC", "papier", "papier2"]

# Array with the amount of routes per type of municipality, loader, waste type and full or half route
L = np.zeros((I, J, K, 2))
# Frequency array
p = np.zeros((I, K))
for r in route_list:
    i = gemeente.index(r["gemeente"])
    j = laders.index(r["lader"])
    k = afval_type.index(r["afval"])
    L[i, j, k, 0] = r["full"]
    L[i, j, k, 1] = r["half"]
    p[i, k] = r["freq"]

# Get the amount of weeks that the schedule should have to be periodic. This will be the lcm of the frequencies
lcm = 1
for i in np.reshape(p, p.size).tolist():
    q = int(i)
    if q > 0:
        lcm = lcm * q // gcd(lcm, q)
W = lcm

# Overlap grijze, groene en oranje weken
ggo_municpalities = ["Enschede", "Oldenzaal", "Losser", "Borne"]

# Municipalities per garage. We need this to minimize movement of trucks between garages.
garage_almelo = ["Almelo"]
garage_hengelo = ["Hengelo", "Hof van Twente", "Oldenzaal", "Borne"]
garage_enschede = ["Enschede", "Losser", "Haaksbergen"]



