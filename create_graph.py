import matplotlib.pyplot as plt
import numpy as np
import geopandas
import networkx as nx
import json


def graph_adjust_almelo(nodes, edges):
    # Add extra nodes and edges to graph that are not in OpenStreetMaps data
    # Ingenhouszlaan
    nodes.append({"ref": 1, "loc": [6.693700, 52.376742]})
    nodes.append({"ref": 2, "loc": [6.695198, 52.375068]})
    edges.append({"ref": 1, "nodes": [1612494744, 1], "road": "residential", "name": "Ingenhouszlaan", "oneway": 0, "start": 1612494744, "urban": "yes"})
    edges.append({"ref": 2, "nodes": [1, 2], "road": "residential", "name": "Ingenhouszlaan", "oneway": 0, "start": 1, "urban": "yes"})
    # Kaamp
    nodes.append({"ref": 3, "loc": [6.654346, 52.308248]})
    edges.append({"ref": 3, "nodes": [46118260, 3], "road": "residential", "name": "Kaamp", "oneway": 0, "start": 46118260, "urban": "no"})
    # Gravenallee
    edges.append({"ref": 4, "nodes": [340062599, 7947996011], "road": "service", "name": "Gravenallee", "oneway": 0, "start": 340062599, "urban": "no"})
    # Lankampsweg
    nodes.append({"ref": 4, "loc": [6.669520, 52.375488]})
    edges.append({"ref": 5, "nodes": [2561875855, 4], "road": "service", "name": "Lankampsweg", "oneway": 0, "start": 2561875855, "urban": "yes"})
    edges.append({"ref": 6, "nodes": [4, 1862038399], "road": "service", "name": "Lankampsweg", "oneway": 0, "start": 4, "urban": "yes"})

    # Adjustments for route ALZL08BRWO, add edges outside almelo area
    nodes.append({"ref": 5, "loc": [6.717742, 52.36690]})
    nodes.append({"ref": 6, "loc": [6.719179, 52.366876]})
    nodes.append({"ref": 7, "loc": [6.722811, 52.367910]})
    nodes.append({"ref": 8, "loc": [6.718974, 52.371692]})
    nodes.append({"ref": 9, "loc": [6.716121, 52.375608]})
    edges.append({"ref": 7, "nodes": [1639392392, 5], "road": "unclassified", "name": "Gravendijk", "oneway": 0, "start": 1639392392,  "urban": "no"})
    edges.append({"ref": 8, "nodes": [5, 6], "road": "unclassified", "name": "Gravendijk", "oneway": 0, "start": 5,  "urban": "no"})
    edges.append({"ref": 9, "nodes": [6, 7], "road": "unclassified", "name": "Gravendijk", "oneway": 0, "start": 6,  "urban": "no"})
    edges.append({"ref": 10, "nodes": [7, 8], "road": "unclassified", "name": "Mekkelenbergweg", "oneway": 0, "start": 7,  "urban": "no"})
    edges.append({"ref": 11, "nodes": [8, 9], "road": "unclassified", "name": "Mekkelenbergweg", "oneway": 0, "start": 8,  "urban": "no"})
    edges.append({"ref": 12, "nodes": [9, 46406156], "road": "unclassified", "name": "Mekkelenbergweg", "oneway": 0, "start": 9,  "urban": "no"})

    # # Nodes and edges for Schierstins hofjes for route ALAL01DRVR
    # # Hof 1
    # nodes.append({"ref": 10, "loc": [6.675185, 52.381335]})
    # nodes.append({"ref": 11, "loc": [6.675108, 52.381715]})
    # edges.append({"ref": 13, "nodes": [46432863, 10], "road": "unclassified", "name": "Schierstins", "oneway": 0, "start": 46432863,  "urban": "yes"})
    # edges.append({"ref": 14, "nodes": [10, 11], "road": "unclassified", "name": "Schierstins", "oneway": 0, "start": 10, "urban": "yes"})
    #
    # # Hof 2
    # nodes.append({"ref": 12, "loc": [6.674279, 52.380265]})
    # nodes.append({"ref": 13, "loc": [6.674279, 52.380596]})
    # nodes.append({"ref": 14, "loc": [6.675064, 52.380625]})
    # edges.append({"ref": 15, "nodes": [12, 13], "road": "unclassified", "name": "Schierstins", "oneway": 0, "start": 12, "urban": "yes"})
    # edges.append({"ref": 16, "nodes": [13, 14], "road": "unclassified", "name": "Schierstins", "oneway": 0, "start": 13, "urban": "yes"})
    #
    #
    # # Hof 3
    # nodes.append({"ref": 15, "loc": [6.673224, 52.380198]})
    # nodes.append({"ref": 16, "loc": [6.673139, 52.380495]})
    # nodes.append({"ref": 17, "loc": [6.672393, 52.380456]})
    # edges.append({"ref": 17, "nodes": [15, 16], "road": "unclassified", "name": "Schierstins", "oneway": 0, "start": 15, "urban": "yes"})
    # edges.append({"ref": 18, "nodes": [16, 17], "road": "unclassified", "name": "Schierstins", "oneway": 0, "start": 16, "urban": "yes"})

    return nodes, edges


def create_graph(nodes, edges):
    G = nx.Graph()
    node_tuples = []
    # Reference dictionary links reference value of node to new node index
    ref_dict = {}
    reverse_ref_dict = {}
    # Make list of tuples with nodes and their attributes
    for n in range(len(nodes)):
        node_tuples.append((n, nodes[n]))
        ref_dict[nodes[n]["ref"]] = n
        reverse_ref_dict[n] = nodes[n]["ref"]

    # Make list of tuples of edges and their attributes
    edge_tuples = []
    for e in range(len(edges)):
        node1, node2 = edges[e]["nodes"]
        edge_tuples.append((ref_dict[node1], ref_dict[node2], edges[e]))

    G.add_nodes_from(node_tuples)
    G.add_edges_from(edge_tuples)

    # Set attributes for number of containers of type per street segment
    nx.set_edge_attributes(G, 0, 'grijs')
    nx.set_edge_attributes(G, 0, 'groen')
    nx.set_edge_attributes(G, 0, 'VEC')
    nx.set_edge_attributes(G, 0, 'papier')

    # Attributes for the route that the edge is a part of
    nx.set_edge_attributes(G, '', 'restroute')
    nx.set_edge_attributes(G, '', 'gftroute')

    return G, ref_dict, reverse_ref_dict


def cull_graph(G, reverse_ref_dict):
    # Get all nodes with degree 2
    nodes_to_remove = [n for n in G.nodes if len(list(G.neighbors(n))) == 2]
    for node in nodes_to_remove:
        edges = G.edges(node)
        # Check if still has two neighbours
        if len(list(edges)) == 2:
            edge1, edge2 = edges
            # If triangle of nodes with degrees 2 then we get errors, we don't remove them
            if not G.has_edge(edge1[1], edge2[1]):
                edge1_data = G.get_edge_data(*edge1)
                edge2_data = G.get_edge_data(*edge2)
                # We keep roads which transition to other streetnames separate
                if edge1_data["name"] == edge2_data["name"] and edge1_data["road"] == edge1_data["road"]:
                    # We add an edge between neighbors (len == 2 so it is correct)
                    adj_nodes = [reverse_ref_dict[edge1[1]], reverse_ref_dict[edge2[1]]]
                    G.add_edge(edge1[1], edge2[1], nodes=adj_nodes, road=edge1_data["road"],name=edge1_data["name"])
                    # And delete the node
                    G.remove_node(node)
    return G


if __name__ == '__main__':
    with open('municipality_graphs/almelo_graph.json', 'r') as f:
        data = json.load(f)

    nodes = data["nodes"]
    edges = data["edges"]

    nodes, edges = graph_adjust_almelo(nodes, edges)

    G, ref_dict, reverse_ref_dict = create_graph(nodes, edges)
    # Create a graph where all extra nodes are removed, i.e. those with 2 edges.
    G_cull = cull_graph(G.copy(), reverse_ref_dict)

    # Get coords of nodes of the graphs
    node_coords = {}
    for n in G.nodes:
        node_coords[n] = nodes[n]["loc"]

    node_orig_coords = {}
    for n in G_cull.nodes:
        node_orig_coords[n] = nodes[n]["loc"]

    # Plot
    f, ax = plt.subplots(1, 2, figsize=(8, 4))
    nx.draw(G, node_coords, ax=ax[0], node_size=5, node_color="b")
    nx.draw(G_cull, node_orig_coords, ax=ax[1], node_size=5, node_color="b")
    plt.show()

    print(len(G.nodes), len(G.edges))
    print(len(G_cull.nodes), len(G_cull.edges))

