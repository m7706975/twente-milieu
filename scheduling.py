import xlsxwriter
from pyscipopt import Model, quicksum
import numpy as np
import pandas as pd
from visualize_schedule import write_schedule, write_route
from schedule_input import *


def MIP_main(alpha, beta, gamma, delta, epsilon, time_main):
    # Initialize the model and add variables
    model = Model("scheduling")

    # These variables will form upper bounds on the amount of loaders used on a day
    c1u = model.addVar("c1u")
    c2u = model.addVar("c2u")
    # Lower bounds
    c1l = model.addVar("c1l")
    c2l = model.addVar("c2l")

    # Variables for upper bounds for garages, order Almelo, Hengelo, Enschede; Achterlader, Zijlader
    cg1u = model.addVar("cg1u")
    cg2u = model.addVar("cg2u")
    cg3u = model.addVar("cg3u")
    cg4u = model.addVar("cg4u")
    cg5u = model.addVar("cg5u")
    cg6u = model.addVar("cg6u")

    # Variable for minimizing amount of same waste at a depot on the same day
    depot_total = model.addVar("depot")

    # Slack variable for monthly Hengelo papier route
    y = {}
    for w in range(W):
        y[w] = model.addVar("Slack" + str(w), ub=1.0)

    # Objective, default is minimize
    model.setObjective(alpha * (beta * c1u - c1l) + (beta * c2u - c2l)
                       + gamma * (cg1u + cg2u + cg3u + cg4u + cg5u + cg6u) + delta * depot_total
                       + epsilon * quicksum(1-y[w] for w in range(W)))

    # Generate a binary variable per route/time slot combination
    x = {}
    # Binary variables for week/day restrictions
    d1 = {}
    d2 = {}

    # Maximum number of routes on a single day
    max_route = int(np.amax(np.sum(L, axis=3)))
    b = np.zeros((I, J, K, max_route))
    for i in range(I):
        for j in range(J):
            for k in range(K):
                for t in range(5 * W):
                    for f in range(2):
                        for l in range(int(L[i, j, k, f])):
                            # Add binary variables for route/day. Also add coefficient for full or half route
                            if f == 0:
                                x[i, j, k, l, t] = model.addVar("x" + str(i) + str(j) + str(k) + str(l) + str(t),
                                                                vtype='B')
                                b[i, j, k, l] = 1
                            else:
                                x[i, j, k, l + int(L[i, j, k, 0]), t] = model.addVar(
                                    "x" + str(i) + str(j) + str(k) + str(l) + str(t), vtype='B')
                                b[i, j, k, l + int(L[i, j, k, 0])] = 0.5

    # Hof van Twente achterlader groen niet meenemen in totaal
    # b[5, 0, 3, 0] = 0

    # Add upper and lower bound constraints
    for t in range(W*5):
        # Upper bounds
        if t % 5 == 2:
            # We try to keep some space on wednesday for monthly route
            model.addCons(quicksum(b[i, 0, k, l]*x[i, 0, k, l, t] for i in range(I) for k in range(K)
                                   for l in range(int(L[i, 0, k, 0]+L[i, 0, k, 1]))) <= c1u - y[t // 5])
        else:
            model.addCons(quicksum(b[i, 0, k, l]*x[i, 0, k, l, t] for i in range(I) for k in range(K)
                                   for l in range(int(L[i, 0, k, 0]+L[i, 0, k, 1]))) <= c1u)
        model.addCons(quicksum(b[i, 1, k, l]*x[i, 1, k, l, t] for i in range(I) for k in range(K)
                               for l in range(int(L[i, 1, k, 0]+L[i, 1, k, 1]))) <= c2u)
        # Lower bounds
        model.addCons(quicksum(b[i, 0, k, l]*x[i, 0, k, l, t] for i in range(I) for k in range(K)
                                   for l in range(int(L[i, 0, k, 0]+L[i, 0, k, 1]))) >= c1l)
        model.addCons(quicksum(b[i, 1, k, l]*x[i, 1, k, l, t] for i in range(I) for k in range(K)
                               for l in range(int(L[i, 1, k, 0]+L[i, 1, k, 1]))) >= c2l)

    # Constraints on frequency
    for i in range(I):
        for j in range(J):
            for k in range(K):
                for l in range(int(L[i, j, k, 0]+L[i, j, k, 1])):
                    model.addCons(quicksum(x[i, j, k, l, t] for t in range(int(5*p[i, k]))) == 1)
                    # Add constraints to get periodic schedule
                    for t in range(int(5*(W-p[i, k]))):
                        model.addCons(x[i, j, k, l, t] == x[i, j, k, l, int(t+5*p[i, k])])

    # Variables for overlapping weeks
    for i in range(I):
        for j in range(J):
            for k in range(K):
                if gemeente[i] in ggo_municpalities or gemeente[i] == "Almelo" or gemeente[i] == "Hengelo":
                    for w in range(W):
                        d1[i, j, k, w] = model.addVar("d1" + str(i) + str(j) + str(k) + str(w), vtype='B')
                        # Constraints to ensure we can only collect in certain weeks
                        for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                            for t in range(5):
                                model.addCons(x[i, j, k, l, 5 * w + t] <= d1[i, j, k, w])

                if gemeente[i] == "Hof van Twente":
                    for t in range(5 * W):
                        d2[i, j, k, t] = model.addVar("d2" + str(i) + str(j) + str(k) + str(t), vtype='B')
                        # Constraints to ensure we can only collect on certain days
                        for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                            model.addCons(x[i, j, k, l, t] <= d2[i, j, k, t])

    # Constraints for overlapping weeks
    grijs = afval_type.index("grijs")
    groen = afval_type.index("groen")
    oranje = afval_type.index("VEC")
    blauw = afval_type.index("papier")
    for i in range(I):
        for j in range(J):
            # Geen overlap grijze, groene en oranje weken
            if gemeente[i] in ggo_municpalities:
                for w in range(W):
                    model.addCons(d1[i, j, grijs, w] + d1[i, j, groen, w] + d1[i, j, oranje, w] <= 1)

            # Geen overlap groene, oranje en blauwe weken
            elif gemeente[i] == "Almelo":
                for w in range(W):
                    model.addCons(d1[i, j, groen, w] + d1[i, j, oranje, w] + d1[i, j, blauw, w] <= 1)

            # Geen overlap grijze, groene en oranje dagen
            elif gemeente[i] == "Hof van Twente":
                for t in range(5*W):
                    model.addCons(d2[i, j, grijs, t] + d2[i, j, groen, t] + d2[i, j, oranje, t] <= 1)

        # Alle groen achterlader/zijlader hengelo in 1 week
        if gemeente[i] == "Hengelo":
            model.addCons(d1[i, 0, groen, 0] + d1[i, 0, groen, 1] <= 1)
            model.addCons(d1[i, 1, groen, 0] + d1[i, 1, groen, 1] <= 1)

    # Get indices of municipality per garage
    g_a = [i for i in range(I) if gemeente[i] in garage_almelo]
    g_h = [i for i in range(I) if gemeente[i] in garage_hengelo]
    g_e = [i for i in range(I) if gemeente[i] in garage_enschede]

    # Add constraints for bounding garages
    for t in range(W*5):
        # Garage Almelo
        model.addCons(quicksum(b[i, 0, k, l]*x[i, 0, k, l, t] for i in g_a for k in range(K)
                               for l in range(int(L[i, 0, k, 0]+L[i, 0, k, 1]))) <= cg1u)
        model.addCons(quicksum(b[i, 1, k, l]*x[i, 1, k, l, t] for i in g_a for k in range(K)
                               for l in range(int(L[i, 1, k, 0]+L[i, 1, k, 1]))) <= cg2u)

        # Garage Hengelo
        model.addCons(quicksum(b[i, 0, k, l]*x[i, 0, k, l, t] for i in g_h for k in range(K)
                               for l in range(int(L[i, 0, k, 0]+L[i, 0, k, 1]))) <= cg3u)
        model.addCons(quicksum(b[i, 1, k, l]*x[i, 1, k, l, t] for i in g_h for k in range(K)
                               for l in range(int(L[i, 1, k, 0]+L[i, 1, k, 1]))) <= cg4u)

        # Garage Enschede
        model.addCons(quicksum(b[i, 0, k, l]*x[i, 0, k, l, t] for i in g_e for k in range(K)
                               for l in range(int(L[i, 0, k, 0]+L[i, 0, k, 1]))) <= cg5u)
        model.addCons(quicksum(b[i, 1, k, l]*x[i, 1, k, l, t] for i in g_e for k in range(K)
                               for l in range(int(L[i, 1, k, 0]+L[i, 1, k, 1]))) <= cg6u)

    # Constraints to prevent too much loaders at depot
    for t in range(5*W):
        # Grijs
        model.addCons(quicksum(b[i, j, k, l]*x[i, j, k, l, t] for i in range(I) for j in range(J) for k in range(3)
                               for l in range(int(L[i, j, k, 0]+L[i, j, k, 1]))) <= depot_total)
        # Groen
        model.addCons(quicksum(b[i, j, k, l]*x[i, j, k, l, t] for i in range(I) for j in range(J) for k in range(3, 5)
                               for l in range(int(L[i, j, k, 0]+L[i, j, k, 1]))) <= depot_total)
        # VEC (Suez Hengelo)
        model.addCons(quicksum(b[i, j, k, l]*x[i, j, k, l, t] for i in range(I) for j in range(J) for k in range(5, 6)
                               for l in range(int(L[i, j, k, 0]+L[i, j, k, 1]))) <= depot_total)
        # Papier (Knol Enschede)
        I_list = [i for i in range(I) if i != 4]
        model.addCons(quicksum(b[i, j, k, l] * x[i, j, k, l, t] for i in I_list for j in range(J) for k in range(6, 8)
                               for l in range(int(L[i, j, k, 0]+L[i, j, k, 1]))) <= depot_total)
        # Papier (Knol Almelo)
        model.addCons(quicksum(b[4, j, k, l] * x[4, j, k, l, t] for j in range(J) for k in range(6, 8)
                               for l in range(int(L[i, j, k, 0]+L[i, j, k, 1]))) <= depot_total)

    # Ensure same collection days between different weeks and waste types, i.e. Ma1 grijs, Ma2 groen, Ma3 VEC, Ma4 groen
    week_municipalities = [0, 2, 3, 4, 6]
    # Waste types
    ggo = [grijs, groen, oranje]
    gob = [groen, oranje, blauw]
    # Tables Enschede and Oldenzaal with current amount of [full, half] routes per loader, waste type and day
    tbl_enschede = [[[[5, 0], [3, 1], [4, 0], [3, 0], [4, 1]],
                     [[4, 0], [3, 0], [3, 1], [3, 0], [3, 0]],
                     [[4, 0], [3, 0], [3, 1], [3, 0], [4, 0]]],
                    [[[4, 0], [5, 0], [3, 1], [5, 0], [5, 0]],
                     [[3, 1], [5, 0], [3, 1], [5, 0], [5, 0]],
                     [[4, 0], [6, 0], [4, 0], [5, 1], [5, 0]]]]

    tbl_oldenzaal = [[[2, 1], [2, 1], [2, 1], [0, 0], [2, 0]],
                     [[3, 0], [3, 0], [2, 1], [0, 0], [2, 1]],
                     [[3, 0], [3, 0], [2, 1], [0, 0], [2, 1]]]

    # Binary variable such that routes have the same collection day between different waste types
    h = {}
    for i in week_municipalities:
        # Losser en Borne
        if i == 0 or i == 2 or i == 3 or i == 6:
            waste_list = ggo
        # Almelo
        elif i == 4:
            waste_list = gob
        # Other municipalities for now
        else:
            print('hai')
            continue
        # Create the variables
        for j in range(J):
            for k in waste_list:
                for l in range(int(L[i, j, k, 0]+L[i, j, k, 1])):
                    for d in range(5):
                        # Add variable per route and week day
                        h[i, j, k, l, d] = model.addVar("h" + str(i) + str(j) + str(k) + str(d), vtype='B')
                    # These constraints ensure we can't set all h variables to 1
                    model.addCons(quicksum(h[i, j, k, l, d] for d in range(5)) <= 1)
            # Need these for overlap constraints
            count1, count2, count3, count4, count5, count6 = 0, 0, 0, 0, 0, 0
            for d in range(5):
                if i == 3 or i == 6:
                    # Route should have some collection day between grijs, groen and oranje
                    for l in range(int(L[i, j, 0, 0] + L[i, j, 0, 1])):
                            model.addCons(h[i, j, grijs, l, d] == h[i, j, groen, l, d])
                            model.addCons(h[i, j, groen, l, d] == h[i, j, oranje, l, d])
                elif i == 4:
                    # Route should have some collection day between groen, oranje and blauw
                    if j == 0:
                        for l in range(int(L[i, j, 5, 0] + L[i, j, 5, 1])):
                                model.addCons(h[i, j, groen, l, d] == h[i, j, oranje, l, d])
                                model.addCons(h[i, j, oranje, l, d] == h[i, j, blauw, l, d])
                    elif j == 1:
                        # Sideloader have 1 extra route for oranje and blauw
                        for l in range(int(L[i, j, 5, 0])):
                                if l != 12:
                                    model.addCons(h[i, j, groen, l, d] == h[i, j, oranje, l, d])
                                model.addCons(h[i, j, oranje, l, d] == h[i, j, blauw, l, d])
                        for l in range(int(L[i, j, 5, 0]), int(L[i, j, 5, 0])+int(L[i, j, 5, 1])):
                                model.addCons(h[i, j, groen, l-1, d] == h[i, j, oranje, l, d])
                                model.addCons(h[i, j, oranje, l, d] == h[i, j, blauw, l, d])
                elif i == 0 or (i == 2 and j != 0):
                    # Same routes for each waste type
                    if i == 0 and j == 0:
                        table = tbl_enschede[0]
                    elif i == 0 and j == 1:
                        table = tbl_enschede[1]
                    else:
                        table = tbl_oldenzaal

                    # if table[0][d] == table[1][d] == table[2][d]:
                    #     # Full routes
                    #     for l in range(table[0][d][0]):
                    #         for day in range(5):
                    #             model.addCons(h[i, j, grijs, count1+l, day] == h[i, j, groen, count2+l, day])
                    #             # print([i, j, grijs, count1+l, day], 'equals', [i, j, groen, count2+l, day])
                    #             model.addCons(h[i, j, groen, count2+l, day] == h[i, j, oranje, count3+l, day])
                    #             # print([i, j, groen, count2+l, day], 'equals', [i, j, oranje, count3+l, day])
                    #     # Half routes
                    #     for l in range(table[0][d][1]):
                    #         for day in range(5):
                    #             model.addCons(h[i, j, grijs, int(L[i, j, grijs, 0])+count4+l, day] == h[i, j, groen, int(L[i, j, groen, 0])+count5+l, day])
                    #             # print([i, j, grijs, int(L[i, j, grijs, 0])+count4+l, day], 'equals', [i, j, groen, int(L[i, j, groen, 0])+count5+l, day])
                    #             model.addCons(h[i, j, groen, int(L[i, j, groen, 0])+count5+l, day] == h[i, j, oranje, int(L[i, j, oranje, 0])+count6+l, day])
                    #             # print([i, j, groen, int(L[i, j, groen, 0])+count5+l, day], 'equals', [i, j, oranje, int(L[i, j, oranje, 0])+count6+l, day])
                    # # Different routes with overlapping area
                    # else:
                    # We first make all routes of the same type overlap
                    # Grijs
                    for l in range(table[0][d][0]):
                        for day in range(5):
                            # print([i, j, grijs, count1+l, day], 'equals', [i, j, groen, count2, day])
                            model.addCons(h[i, j, grijs, count1+l, day] == h[i, j, groen, count2, day])
                    for l in range(table[0][d][1]):
                        for day in range(5):
                            # print([i, j, grijs, int(L[i, j, grijs, 0])+count4+l, day], 'equals', [i, j, groen, count2, day])
                            model.addCons(h[i, j, grijs, int(L[i, j, grijs, 0])+count4+l, day] == h[i, j, groen, count2, day])
                    # Groen
                    for l in range(table[1][d][0]):
                        for day in range(5):
                            # print([i, j, groen, count2 + l, day], 'equals', [i, j, oranje, count3, day])
                            model.addCons(h[i, j, groen, count2 + l, day] == h[i, j, oranje, count3, day])
                    for l in range(table[1][d][1]):
                        for day in range(5):
                            # print([i, j, groen, int(L[i, j, groen, 0]) + count5 + l, day], 'equals', [i, j, oranje, count3, day])
                            model.addCons(h[i, j, groen, int(L[i, j, groen, 0]) + count5 + l, day] == h[i, j, oranje, count3, day])
                    # Oranje
                    for l in range(table[2][d][0]):
                        for day in range(5):
                            # print([i, j, oranje, count3 + l, day], 'equals', [i, j, grijs, count1, day])
                            model.addCons(h[i, j, oranje, count3 + l, day] == h[i, j, grijs, count1, day])
                    for l in range(table[2][d][1]):
                        for day in range(5):
                            # print([i, j, oranje, int(L[i, j, oranje, 0]) + count6 + l, day], 'equals', [i, j, grijs, count1, day])
                            model.addCons(h[i, j, oranje, int(L[i, j, oranje, 0]) + count6 + l, day] == h[i, j, grijs, count1, day])

                        # Then the first routes on each day must overlap between the waste types, as a result all routes
                        # overlap

                    count1 += table[0][d][0]
                    count2 += table[1][d][0]
                    count3 += table[2][d][0]
                    count4 += table[0][d][1]
                    count5 += table[1][d][1]
                    count6 += table[2][d][1]

    # Add constraints such that h variables limit available days of x variables.
    for i in week_municipalities:
        if i == 0 or i == 2 or i == 3 or i == 6:
            waste_list = ggo
        elif i == 4:
            waste_list = gob
        else:
            print('hey')
            continue
        for j in range(J):
            for k in waste_list:
                for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                    for t in range(int(5*p[i, k])):
                        model.addCons(x[i, j, k, l, t] <= h[i, j, k, l, t % 5])

    # Overlap constraints for Enschede
    # For each of 11 routes, figure out with which original day we can't overlap, then implement constraint x1+x2<=1

    # Route EN A11 P4D. Can't overlap with A maandag, A dinsdag, Z maandag, Z vrijdag
    for t in range(int(5*p[0, 0])):
        model.addCons(x[0, 0, 6, 0, t] + x[0, 0, 0, 0, t] <= 1)
        model.addCons(x[0, 0, 6, 0, t] + x[0, 0, 0, 5, t] <= 1)
        model.addCons(x[0, 0, 6, 0, t] + x[0, 1, 0, 0, t] <= 1)
        model.addCons(x[0, 0, 6, 0, t] + x[0, 1, 0, 17, t] <= 1)

    # Bounding the solution from below by its optimum that we know beforehand
    model.addCons(c1u >= 9)
    model.addCons(c2u >= 22)
    model.addCons(c1l <= 8)
    model.addCons(c2l <= 21)

    # Best bounds for garage and depot
    model.addCons(cg1u >= 3)
    model.addCons(cg2u >= 4)
    model.addCons(cg3u >= 2)
    model.addCons(cg4u >= 9)
    model.addCons(cg5u >= 5)
    model.addCons(cg6u >= 10)
    model.addCons(depot_total >= 10)

    # Put limits on the variables, otherwise we might get weird behaviour for second solve
    model.addCons(cg1u <= 1000)
    model.addCons(cg2u <= 1000)
    model.addCons(cg3u <= 1000)
    model.addCons(cg4u <= 1000)
    model.addCons(cg5u <= 1000)
    model.addCons(cg6u <= 1000)
    model.addCons(depot_total <= 1000)

    # Set time limit and optimize
    model.setParam('limits/time', time_main)

    model.optimize()
    sol = model.getBestSol()
    for t in range(int(5*p[0, 0])):
        print(sol[x[0, 0, 6, 0, t]], sol[x[0, 0, 0, 0, t]])
        print(sol[x[0, 0, 6, 0, t]], sol[x[0, 0, 0, 5, t]])
        print(sol[x[0, 0, 6, 0, t]], sol[x[0, 1, 0, 0, t]])
        print(sol[x[0, 0, 6, 0, t]], sol[x[0, 1, 0, 17, t]])

    variable_list = [x, d1, d2, y, h, c1u, c2u, c1l, c2l, cg1u, cg2u, cg3u, cg4u, cg5u, cg6u, depot_total]

    return model, b, variable_list


def MIP_second_obj(model, b, variable_list, garage, depot, primal_bound, time_secondary):
    # This MIP optimmizes the second objective after having solved main objective
    [x, d1, d2, y, h, c1u, c2u, c1l, c2l, cg1u, cg2u, cg3u, cg4u, cg5u, cg6u, depot_total] = variable_list

    # Get solution and primal bound
    sol = model.getBestSol()
    # primal_bound = model.getPrimalbound()

    # Store solution of binary variables in new dicts
    x_p = {}
    d1_p = {}
    d2_p = {}
    h_p = {}
    for i in range(I):
        for j in range(J):
            for k in range(K):
                for w in range(W):
                    if gemeente[i] in ggo_municpalities or gemeente[i] == "Almelo" or gemeente[i] == "Hengelo":
                        d1_p[i, j, k, w] = sol[d1[i, j, k, w]]
                    for t in range(5):
                        for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                            x_p[i, j, k, l, 5 * w + t] = sol[x[i, j, k, l, 5 * w + t]]
                        if gemeente[i] == "Hof van Twente":
                            d2_p[i, j, k, 5 * w + t] = sol[d2[i, j, k, 5 * w + t]]
                # Variables for same collection days
                # Losser and Borne
                if (i == 0 or i == 2 or i == 3 or i == 6) and k in [0, 3, 5]:
                    for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                        for d in range(5):
                            h_p[i, j, k, l, d] = sol[h[i, j, k, l, d]]
                # Almelo
                if i == 4 and k in [3, 5, 6]:
                    for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                        for d in range(5):
                            h_p[i, j, k, l, d] = sol[h[i, j, k, l, d]]

    y_p = {}
    for w in range(W):
        y_p[w] = sol[y[w]]

    [cg1u_f, cg2u_f, cg3u_f, cg4u_f, cg5u_f, cg6u_f, depo_total_f] = \
        [sol[cg1u], sol[cg2u], sol[cg3u], sol[cg4u], sol[cg5u], sol[cg6u], sol[depot_total]]

    # Now try to optimize secondary objective by transforming the model
    model.freeTransform()

    # First we take the optimal solution of the initial solve. We change the upper bound variables for the secondary
    # objective to still be feasible but improved.
    s = model.createSol()
    for i in range(I):
        for j in range(J):
            for k in range(K):
                for l in range(int(L[i, j, k, 0]+L[i, j, k, 1])):
                    for w in range(W):
                        if gemeente[i] in ggo_municpalities or gemeente[i] == "Almelo" or gemeente[i] == "Hengelo":
                            model.setSolVal(s, d1[i, j, k, w], d1_p[i, j, k, w])
                        for t in range(5):
                            model.setSolVal(s, x[i, j, k, l, 5*w+t], x_p[i, j, k, l, 5*w+t])
                            if gemeente[i] == "Hof van Twente":
                                model.setSolVal(s, d2[i, j, k, 5*w+t], d2_p[i, j, k, 5*w+t])
                # GGO
                if (i == 0 or i == 2 or i == 3 or i == 6) and k in [0, 3, 5]:
                    for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                        for d in range(5):
                            model.setSolVal(s, h[i, j, k, l, d], h_p[i, j, k, l, d])
                # Almelo GOB
                if i == 4 and k in [3, 5, 6]:
                    for l in range(int(L[i, j, k, 0] + L[i, j, k, 1])):
                        for d in range(5):
                            model.setSolVal(s, h[i, j, k, l, d], h_p[i, j, k, l, d])

    for w in range(W):
        model.setSolVal(s, y[w], y_p[w])

    # Set upper and lower bounds for optimal daily
    model.setSolVal(s, c1u, 9)
    model.setSolVal(s, c2u, 22)
    model.setSolVal(s, c1l, 8)
    model.setSolVal(s, c2l, 21)
    # Set upper bound variables for garage and depot
    model.setSolVal(s, cg1u, min(9, cg1u_f))
    model.setSolVal(s, cg2u, min(22, cg2u_f))
    model.setSolVal(s, cg3u, min(9, cg3u_f))
    model.setSolVal(s, cg4u, min(22, cg4u_f))
    model.setSolVal(s, cg5u, min(9, cg5u_f))
    model.setSolVal(s, cg6u, min(22, cg6u_f))
    model.setSolVal(s, depot_total, min(31, depo_total_f))
    model.addSol(s)

    # We add a constraint such that the optimal solution of the first solve is respected
    model.addCons(alpha * (beta*c1u - c1l) + (beta*c2u - c2l)
                  + epsilon * quicksum((1-y[w]) for w in range(W)) <= primal_bound)

    # We change the objective to the secondary one
    model.setObjective(garage*(cg1u + cg2u + cg3u + cg4u + cg5u + cg6u) + depot*depot_total)

    # Change time limit, call heuristc which tries to first decrease the upper bound variables
    model.setParam('limits/time', time_secondary)
    model.setParam('limits/gap', 0.02)

    model.writeProblem('schedule_lp.lp')
    model.optimize()

    return model


def print_solution(model, variable_list):
    # Print the best solution from the model
    sol = model.getBestSol()
    [c1u, c2u, c1l, c2l, cg1u, cg2u, cg3u, cg4u, cg5u, cg6u, depot_total] = variable_list[5:]

    # Main variables
    print("Achterlader upper bound: {}".format(sol[c1u]))
    print("Zijlader upper bound: {}".format(sol[c2u]))
    print("Achterlader lower bound: {}".format(sol[c1l]))
    print("Zijlader lower bound: {}".format(sol[c2l]))
    # Limit movement garage variables
    print("Almelo achterlader upper bound: {}".format(sol[cg1u]))
    print("Almelo zijlader upper bound: {}".format(sol[cg2u]))
    print("Hengelo achterlader upper bound: {}".format(sol[cg3u]))
    print("Hengelo zijlader upper bound: {}".format(sol[cg4u]))
    print("Enschede achterlader upper bound: {}".format(sol[cg5u]))
    print("Enschede zijlader upper bound: {}".format(sol[cg6u]))
    print("Maximum lader at depot: {}".format(sol[depot_total]))


def write_solution(model, b, variable_list):
    sol = model.getBestSol()
    x = variable_list[0]
    # To visualize the solution, we first need to extract it and store it in dataframes
    # schedule_dict will contain the number of routes per municipality, waste type, loader type and day.
    schedule_dict = {}
    # route_dict will contain the schedule of each individual route
    route_dict = {}
    route_type = []
    gem = []
    route_type2 = []
    gem2 = []
    route_name = []
    for t in range(5 * W):
        route_sum = []
        route_day = []
        for i in range(I):
            for j in range(J):
                for k in range(K):
                    route_sum.append(sol[quicksum(b[i, j, k, l]*x[i, j, k, l, t] for l in range(int(L[i, j, k, 0]+L[i, j, k, 1])))])
                    # Add a column containing type of route before first day
                    # for l in range(int(L[i, j, k, 0]+L[i, j, k, 1])):
                    #     route_day.append(sol[x[i, j, k, l, t]])
                    #     if t == 0:
                    #         route_type2.append(laders[j] + " " + afval_type[k])
                    #         gem2.append(gemeente[i])
                    #         route_name.append(repr(x[i, j, k, l, t])[:-1])
                    #         if l == 0:
                    if t == 0:
                        route_type.append(laders[j] + " " + afval_type[k])
                        gem.append(gemeente[i])
        if t == 0:
            schedule_dict["Gemeente"] = gem
            schedule_dict[""] = route_type
            route_dict["Gemeente"] = gem2
            route_dict[""] = route_type2
            route_dict["Route"] = route_name

        # If monday, tuesday, ....
        if t % 5 == 0:
            schedule_dict["Ma" + str(t//5+1)] = route_sum
            route_dict["Ma" + str(t // 5 + 1)] = route_day
        elif t % 5 == 1:
            schedule_dict["Di" + str(t//5+1)] = route_sum
            route_dict["Di" + str(t // 5 + 1)] = route_day
        elif t % 5 == 2:
            schedule_dict["Wo" + str(t//5+1)] = route_sum
            route_dict["Wo" + str(t // 5 + 1)] = route_day
        elif t % 5 == 3:
            schedule_dict["Do" + str(t//5+1)] = route_sum
            route_dict["Do" + str(t // 5 + 1)] = route_day
        else:
            schedule_dict["Vr" + str(t//5+1)] = route_sum
            route_dict["Vr" + str(t // 5 + 1)] = route_day
            # Empty column for weekend
            schedule_dict["Weekend" + str(t//5+1)] = np.nan
            route_dict["Weekend" + str(t // 5 + 1)] = np.nan

    df = pd.DataFrame(schedule_dict)
    # Replace all empty values by None
    df = df.replace(0, None)
    df = df.replace(np.nan, None)
    # Drop rows which only contain None
    df = df.iloc[df.iloc[:, 2:].dropna(how='all').index.values].reset_index(drop=True)

    # Write to Excel file
    write_schedule(df, gemeente, W, "Schedule.xlsx")

    # ------------ Optional second Excel file ---------------
    # df2 = pd.DataFrame(route_dict)
    # df2 = df2.replace(0, None)
    #
    # # Determine on which days the waste is collected for each route
    # waste_day = []
    # weekdays = ["Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag"]
    # r = 0
    # Loop over all routes
    # for i in range(I):
    #     for j in range(J):
    #         for k in range(K):
    #             for l in range(int(L[i, j, k, 0]+L[i, j, k, 1])):
    #                 # Loop over days
    #                 for t in range(int(5*p[i, k])):
    #                     # If waste is collected on day t, do
    #                     if sol[x[i, j, k, l, t]] == 1:
    #                         weeks = ''.join("%s, " % int((t // 5) + week*p[i, k] + 1) for week in range(int(W/p[i, k])))
    #                         day = weekdays[(t % 5)]
    #                 waste_day.append("Weken " + weeks + day)
    #                 r += 1

    # df2.insert(3, "Ophaaldagen", waste_day)

    # Write total schedule and schedule per route to Excel files
    # write_route(df2, I, J, K, W, L, gemeente, "Route.xlsx")

if __name__ == '__main__':
    # Set parameters for the objective function
    alpha = 1
    beta = 3
    gamma = 0
    delta = 0
    epsilon = 0.01

    # Time for MIP solvers (s)

    # Solve initial using MIP
    model, b, variable_list = MIP_main(alpha, beta, gamma, delta, epsilon, 900)
    primal_bound = model.getPrimalbound()
    print_solution(model, variable_list)

    # Second objective
    # write_solution(model, b, variable_list)

    # model = MIP_second_obj(model, b, variable_list, 1, 0, primal_bound, 900)
    # print_solution(model, variable_list)
    #
    # print(model.getPrimalbound())
    # print(model.getObjective())
    # print(model.getObjVal())
    # model.printBestSol()

    # Now also depot
    # model = MIP_second_obj(model, b, variable_list, 1, 1, primal_bound, 300)
    # print_solution(model, variable_list)

    # Write solution
    # write_solution(model, b, variable_list)
