import osmnx as ox
import osmium as o
import shapely.wkb as wkblib
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import json


class RoadNetworkHandler(o.SimpleHandler):
    def __init__(self):
        super(RoadNetworkHandler, self).__init__()
        self.roads = []
        self.nodes = []
        self.double = 0
        self.road_types = []
        self.restrictions = []

    def way(self, w):
        # Only select roads of certain type
        if 'highway' in w.tags:
            if w.tags['highway'] in road_types:
                # Check for all road types
                if w.tags['highway'] not in self.road_types:
                    self.road_types.append(w.tags['highway'])
                cond = False
                # Check if way is in municipality
                for n in w.nodes:
                    point = Point(n.location.lon, n.location.lat)
                    if polygon.contains(point):
                        cond = True
                # If in municipality, add relevant nodes and edges
                if cond:
                    count = 0
                    for n in w.nodes:
                        # Check which nodes are already included
                        ref_nodes = [ref_dict["ref"] for ref_dict in self.nodes]
                        # Only add new nodes to the graph
                        if n.ref not in ref_nodes:
                            self.nodes.append({"ref": n.ref, "loc": (n.location.lon, n.location.lat)})
                        else:
                            self.double += 1
                        # Add edges
                        if count > 0:
                            edge = {'ref': w.id, 'nodes': (n_prev.ref, n.ref), 'road': w.tags['highway']}
                            if 'name' in w.tags:
                                edge['name'] = w.tags['name']
                            else:
                                edge['name'] = ''
                            if 'oneway' in w.tags:
                                if w.tags['oneway'] == 'yes':
                                    edge['oneway'] = 1
                                    edge['start'] = n_prev.ref
                                elif w.tags['oneway'] == -1:
                                    edge['oneway'] = -1
                                    edge['start'] = n.ref
                                else:
                                    edge['oneway'] = 0
                                    edge['start'] = n_prev.ref
                            else:
                                edge['oneway'] = 0
                                edge['start'] = n_prev.ref
                            self.roads.append(edge)
                        n_prev = n
                        count += 1

    def relation(self, r):
        # Here we add all relations that add a turn restrcitions
        if 'restriction' in r.tags:
            rel_dict = {'ref': r.id}
            members = []
            for member in r.members:
                members.append({'ref': member.ref, 'role': member.role, 'type': member.type})
            rel_dict['members'] = members

            rel_dict['restriction'] = r.tags['restriction']
            if 'except' in r.tags:
                rel_dict['except'] = r.tags['except']

            self.restrictions.append(rel_dict)


def main(osmfile, road_types):
    h = RoadNetworkHandler()
    # As we need the geometry, the node locations need to be cached. Therefore
    # set 'locations' to true.
    h.apply_file(osmfile, locations=True)

    print('Number of nodes: ', len(h.nodes))
    print('Number of doubled nodes: ', h.double)
    print('Number of roads: ', len(h.roads))
    print('Types of roads: ', h.road_types)
    print('Number of restrictions:', len(h.restrictions))

    return {'nodes': h.nodes, 'edges': h.roads}, h.restrictions


if __name__ == '__main__':
    # The types of roads we want in our road network
    road_types = ["motorway", "motorway_link", "trunk", "trunk_link", "primary", "secondary", "tertiary",
                  "primary_link", "secondary_link", "tertiary_link", "residential", "service", "unclassified"]
    optional = ["track", "living_street", "construction", "busway", "road"]
    road_types = road_types + optional

    municipality = 'almelo'
    # Open polygon shape of current municipality or route region
    with open('municipality_polygon/' + municipality + '_shape.json', 'r') as f:
        data = json.load(f)
    data_tuples = [tuple(x) for x in data["coordinates"][0][0]]
    polygon = Polygon(data_tuples)

    # Open polygon shape of urban area (which has slower driving speed)
    with open('municipality_polygon/' + municipality + '_bebouwd.json', 'r') as f:
        data2 = json.load(f)
    data_tuples2 = [tuple(x) for x in data2["coordinates"][0]]
    polygon2 = Polygon(data_tuples)

    # Obtain data on the road network
    graph_data, relations = main("overijssel-latest.osm.pbf", road_types)

    # Get nodes and edges
    nodes = graph_data['nodes']
    edges = graph_data['edges']

    # For each edge we want to specify if it is an urban area, because this influences the driving speed
    # First obtain location of each node in a dict
    loc_dict = {}
    node_refs = []
    edge_refs = []
    for node in nodes:
        loc_dict[node['ref']] = node['loc']
        node_refs.append(node['ref'])
    # Check if edge is in urban area, and add according attribute
    for edge in edges:
        if edge['ref'] not in edge_refs:
            edge_refs.append(edge['ref'])
        node1, node2 = edge['nodes']
        loc1 = Point(loc_dict[node1][0], loc_dict[node1][1])
        loc2 = Point(loc_dict[node2][0], loc_dict[node2][1])
        if polygon2.contains(loc1) and polygon2.contains(loc2):
            edge["urban"] = "yes"
        elif not polygon2.contains(loc1) and not polygon2.contains(loc2):
            edge["urban"] = "no"
        else:
            edge["urban"] = "mixed"

    # Here we keep the relations that are relevant to the road network in Almelo
    restrictions = []
    for rel in relations:
        cond = True
        for member in rel['members']:
            if member['type'] == 'n':
                if member['ref'] not in node_refs:
                    cond = False
            elif member['type'] == 'w':
                if member['ref'] not in edge_refs:
                    cond = False
        # cond=True if all members of the relation are relating to Almelo
        if cond:
            restrictions.append(rel)

    print(len(restrictions))
    graph_data['restrictions'] = restrictions

    with open('municipality_graphs/' + municipality + '_graph2.json', 'w') as outfile:
        json.dump(graph_data, outfile)

