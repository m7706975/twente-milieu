import pandas as pd
import networkx as nx
import json
from math import radians, cos, sin, asin, sqrt
import matplotlib.pyplot as plt
from create_graph import create_graph


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance in kilometers between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles. Determines return value units.
    return c * r


if __name__ == '__main__':
    # Load road network graph
    with open('municipality_graphs/almelo_graph.json', 'r') as f:
        data = json.load(f)

    # Create graph from file
    nodes = data["nodes"]
    edges = data["edges"]
    restrictions = data["restrictions"]
    G, ref_dict, reverse_ref_dict = create_graph(nodes, edges)
    node_coords = {}
    for n in G.nodes:
        node_coords[n] = nodes[n]["loc"]

    # Read file with addresses per route for achterlader rest
    df = pd.read_csv("csv_files/Containers_-1_Almelo_Rest_Achterlader.csv", sep=";")
    df = df[df["gebiednaam"] == "AL AL08 DRVR"]
    # Streets for this route
    streets = pd.unique(df["straat"].dropna().values.ravel())

    # Street path through route AL AL08 DRVR
    street_path = ['Rietstraat', 'Violierstraat', 'Bornsestraat', 'Hazelaarstraat', 'Haagbeuk', 'Hazelaarstraat',
                   'Deldensestraat', 'Weggelerstraat', 'Bornsestraat', 'Smaragdstraat', 'Diamantstraat',
                   'Briljantstraat', 'Bornsestraat', 'Christoffelstraat', 'Deldensestraat', 'Nieuwe Planthofsweg',
                   'Planthofsweg', 'Deldensestraat', 'Kamperfoeliestraat', 'Clematisstraat', 'Hulststraat',
                   'Kroosstraat', 'Klimopstraat', 'Hoornbladstraat', 'Goudenregenstraat', 'Kroosstraat',
                   'Seringenstraat', 'Hoornbladstraat', 'Rietstraat', 'Kroosstraat', 'Jasmijnstraat', 'Hoornbladstraat',
                   'Lavendelstraat', 'Hoornbladstraat', 'Deldensestraat']

    # Select all nodes and edges that could be on the route
    nodes_in_route = {}
    edges_in_route = []
    for e in G.edges:
        edge_data = G.get_edge_data(*e)
        if edge_data["name"] in street_path:
            edges_in_route.append(e)
            for n in edge_data["nodes"]:
                if ref_dict[n] not in nodes_in_route.keys():
                    nodes_in_route[ref_dict[n]] = {"streets": [edge_data["name"]]}
                    nodes_in_route[ref_dict[n]]["route_node"] = []
                elif edge_data["name"] not in nodes_in_route[ref_dict[n]]:
                    nodes_in_route[ref_dict[n]]["streets"] += [edge_data["name"]]

    # Loop over the streets in the path
    node_list = []
    for i in range(len(street_path) - 1):
        street1 = street_path[i]
        street2 = street_path[i + 1]
        found = False
        # Loop over all nodes in this route
        for n, d in nodes_in_route.items():
            # print(street1, street2, d["streets"])
            if street1 in d["streets"] and street2 in d["streets"]:
                nodes_in_route[n]["route_node"] += (reverse_ref_dict[n], street1, street2)
                # print((reverse_ref_dict[n], street1, street2))
                node_list.append((reverse_ref_dict[n], street1, street2))
                found = True
        # if not found:
        #     print(street1, street2)

    # node_refs = [46277696, 46284086, 46282199, 46279687, 46279687, 46270902, 3201793752, 46275951, 2035570719, 2035570708,
    #  2035570695, 46279734, 46268877, 511043439, 46253282, 46255715, 46262511, 46262994, 46275852, 46276773, 46267345,
    #  46263797, 46274569, 46275205, 46264240, 46264077, 46274092, 46274095, 46263796, 46268546, 46268546, 46269488,
    #  46270288]

    node_refs = [(46277696, 'Rietstraat', 'Violierstraat'), (46284086, 'Violierstraat', 'Bornsestraat'),
     (46282199, 'Bornsestraat', 'Hazelaarstraat'), (46279687, 'Hazelaarstraat', 'Haagbeuk'),
     (46279686, 'Haagbeuk', 'Haagbeuk'), (46279687, 'Haagbeuk', 'Hazelaarstraat'),
     (46270902, 'Hazelaarstraat', 'Deldensestraat'), (3201793752, 'Deldensestraat', 'Weggelerstraat'),
     (46275951, 'Weggelerstraat', 'Bornsestraat'), (2035570719, 'Bornsestraat', 'Smaragdstraat'),
     (2035570708, 'Smaragdstraat', 'Smaragdstraat'), (2035570695, 'Smaragdstraat', 'Diamantstraat'),
     (2035570694, 'Diamantstraat', 'Briljantstraat'), (2035570704, 'Briljantstraat', 'Bornsestraat'),
     (46279734, 'Bornsestraat', 'Christoffelstraat'), (46268877, 'Christoffelstraat', 'Deldensestraat'),
     (511043439, 'Deldensestraat', 'Nieuwe Planthofsweg'), (46253282, 'Nieuwe Planthofsweg', 'Planthofsweg'),
     (46255715, 'Planthofsweg', 'Deldensestraat'), (46262511, 'Deldensestraat', 'Kamperfoeliestraat'),
     (46262777, 'Kamperfoeliestraat', ''), (46265438, '', 'Hulststraat'),
     (46275852, 'Hulststraat', 'Kroosstraat'), (46276773, 'Kroosstraat', 'Klimopstraat'),
     (46267345, 'Klimopstraat', 'Hoornbladstraat'), (46263797, 'Hoornbladstraat', 'Goudenregenstraat'),
     (46274569, 'Goudenregenstraat', 'Kroosstraat'), (46275205, 'Kroosstraat', 'Seringenstraat'),
     (46264240, 'Seringenstraat', 'Hoornbladstraat'), (46264077, 'Hoornbladstraat', 'Rietstraat'),
     (46274092, 'Rietstraat', 'Kroosstraat'), (46274095, 'Kroosstraat', 'Jasmijnstraat'),
     (46263796, 'Jasmijnstraat', 'Hoornbladstraat'), (46268546, 'Hoornbladstraat', 'Lavendelstraat'),
     (3782555304, 'Lavendelstraat', 'Lavendelstraat'), (46268546, 'Lavendelstraat', 'Hoornbladstraat'),
     (46270288, 'Hoornbladstraat', 'Deldensestraat'), (4572787298, 'Deldensestraat', 'Deldensestraat')]

    # Inital node in rietstraat
    path_list = []
    start_node = ref_dict[46264077]
    for n in range(len(node_refs)):
        # Next node that we need to visit from start node that is on same street
        next_node = ref_dict[node_refs[n][0]]
        # We start searching from start_node to next_node along the same street by visiting nodes inbetween
        found = False
        # We keep lists with all nodes we still have to visit and those that have been visited
        queue = [start_node]
        visited = []
        # This dictionary gives per node the previous node from which it was reached. This is used to determine path
        path = {}
        while not found:
            current_node = queue.pop(0)
            visited.append(current_node)
            for e in G.edges(current_node):
                # Here we look through all neighbours on the same street
                if G[e[0]][e[1]]['name'] == node_refs[n][1]:
                    if e[1] not in queue and e[1] not in visited:
                        path[e[1]] = current_node
                        queue.append(e[1])
                        # Check if we have found the next node
                        if e[1] == next_node:
                            found = True

        # Check the path backwards from last node
        path_bool = True
        temp_list = []
        current_node = next_node
        while path_bool:
            # print(reverse_ref_dict[current_node], reverse_ref_dict[path[current_node]], G[path[current_node]][current_node]['name'])
            temp_list += [(path[current_node], current_node)]
            # Take previous node in path, quit loop if it is start_node
            current_node = path[current_node]
            if current_node == start_node:
                path_bool = False

        path_list += reversed(temp_list)
        start_node = next_node

    # print(path_list)
    total_dist = 0
    for edge in path_list:
        lon1, lat1 = G.nodes[edge[0]]['loc']
        lon2, lat2 = G.nodes[edge[1]]['loc']
        total_dist += haversine(lon1, lat1, lon2, lat2)
    print(total_dist)

    # Color nodes and edges in the route
    node_colors = []
    edge_colors = []
    edge_widths = []
    for n in G.nodes:
        if n in nodes_in_route.keys():
            if reverse_ref_dict[n] in [node[0] for node in node_refs]: #len(nodes_in_route[n]["route_node"]) >= 1:
                node_colors.append('red')
            else:
                node_colors.append('blue')
        else:
            node_colors.append('blue')
    for e in G.edges:
        # print(e)
        if (e[0], e[1]) in path_list or (e[1], e[0]) in path_list:
            edge_colors.append('red')
            edge_widths.append(2)
        else:
            edge_colors.append('black')
            edge_widths.append(1)

    nx.draw(G, node_coords, node_size=3, node_color=node_colors, edge_color=edge_colors, width=edge_widths)
    plt.show()
